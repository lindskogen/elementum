package se.chalmers.tda367.grupp12.elementum.utils;

import org.lwjgl.opengl.Display;

/**
 * Date: 4/10/13
 * Time: 6:04 PM
 * This class provides the frame rate to the window title frame
 */
public class FPSManager {
    static private volatile FPSManager instance = null;

    private static final int preferredFPS = 60;
    private int FPS = preferredFPS;
    private int calcs = 0;
    private long startTime;
    private long lastUpdate;

    static public FPSManager getSharedInstance() {
        if (instance == null) {
            instance = new FPSManager();
        }
        return instance;
    }
    private FPSManager(){
        lastUpdate=startTime = System.currentTimeMillis();
    }
    public void update(){
        Long tempt = System.currentTimeMillis();
        float delta = (float) (tempt - lastUpdate);
        calcs++;
        if(tempt -startTime> 1000){
            FPS = (int)(calcs/((tempt-startTime)  /1000));
            calcs = 0;
            startTime = tempt;
            Display.setTitle("FPS: " + Integer.toString(FPS) + "   Delta: " + Float.toString(delta));
        }
        lastUpdate = tempt;
    }
    public float getFPS(){
        return FPS;
    }
    public long getElapsedTimeMillis(){
        return (long) (1000 / FPSManager.getSharedInstance().getFPS());
    }
}
