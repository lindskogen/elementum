package se.chalmers.tda367.grupp12.elementum.utils;


import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.util.WaveData;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.openal.AL10.*;

public class Audio {
    private static final List<Track> tracks= new ArrayList<Track>();
    public static class Track {
        private final String path;
        boolean loop = true;
        private final int id;
        private float pitch = 1.0f;
        private float gain = 1.0f;
        public Track(String path, float pitch, float gain,boolean loop){
            this(path);
            this.loop=loop;
            this.pitch=pitch;
            this.gain=gain;
        }
        public Track(String path){
            //this.path=path;
	        this.path = FileManager.getInstance().getSoundEffect(path);
	        id = tracks.size();
            tracks.add(this);
        }
    }

    /*Tracks*/
    public static final Track MAIN = new Track("main.wav",1,1,true);
    public static final Track LEVEL = new Track("DST-ReturnOfTowerDefenseTheme.wav",1,1,true);
    public static final Track FIRE_ELEMENT = new Track("fire_element.wav",1,0.5f,true);
    public static final Track WATER_ELEMENT = new Track("water_element.wav",1,0.5f,true);
    public static final Track WIND_ELEMENT = new Track("wind_element.wav",1,0.5f,true);
    public static final Track SMASH = new Track("crash.wav",1,1,false);
    public static final Track COLLECT = new Track("collected.wav",1,1,false);

    /** Buffers hold sound data. */
    private final IntBuffer buffer;

    /** Sources are points emitting sound. */
    private final IntBuffer source;

    /** Position of the source sound. */
    private final FloatBuffer sourcePos = BufferUtils.createFloatBuffer(3).put(new float[] { 0.0f, 0.0f, 0.0f });

    /** Velocity of the source sound. */
    private final FloatBuffer sourceVel = BufferUtils.createFloatBuffer(3).put(new float[] { 0.0f, 0.0f, 0.0f });

    /** Position of the listener. */
    private final FloatBuffer listenerPos = BufferUtils.createFloatBuffer(3).put(new float[] { 0.0f, 0.0f, 0.0f });

    /** Velocity of the listener. */
    private final FloatBuffer listenerVel = BufferUtils.createFloatBuffer(3).put(new float[] { 0.0f, 0.0f, 0.0f });

    /** Orientation of the listener. (first 3 elements are "at", second 3 are "up") */
    private final FloatBuffer listenerOri =
            BufferUtils.createFloatBuffer(6).put(new float[] { 0.0f, 0.0f, 0.0f,  0.0f, 1.0f, 0.0f });

    static private volatile Audio instance = null;
    static public Audio getSharedInstance() {
        if (instance == null) {
            instance = new Audio();
        }
        return instance;
    }

    private Audio(){

        /** Buffers hold sound data. */
        buffer = BufferUtils.createIntBuffer(tracks.size());

        /** Sources are points emitting sound. */
        source = BufferUtils.createIntBuffer(tracks.size());

        /* init al */
        try {
            AL.create();
        } catch (LWJGLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        /* rewind default buffers */
        source.rewind();
        buffer.rewind();
        sourcePos.rewind();
        sourceVel.rewind();
        listenerOri.rewind();
        listenerPos.rewind();
        listenerVel.rewind();

        /* gen al buffers */
        alGenBuffers(buffer);

        loadTracks();
        setListener(listenerPos, listenerVel, listenerOri);

    }
    public void play(Track t){
        alSourcePlay(source.get(t.id));
    }
    public void stop(Track t){
        alSourceStop(source.get(t.id));
    }
    void setListener(FloatBuffer lp, FloatBuffer lv, FloatBuffer lo){
        alListener(AL_POSITION,    lp);
        alListener(AL_VELOCITY,    lv);
        alListener(AL_ORIENTATION, lo);

    }
    private void loadTracks(){
        try {
            for(Track t:tracks){
                WaveData waveFile = WaveData.create(new BufferedInputStream(new FileInputStream(t.path)));
                alBufferData(buffer.get(t.id), waveFile.format, waveFile.data, waveFile.samplerate);
                waveFile.dispose();

                //System.out.println("Loading file " + t.path + ": " + alGetString(alGetError()));
            }

            alGenSources(source);

            for(Track t:tracks){
                int i = t.id;
                alSourcei(source.get(i), AL_BUFFER,   buffer.get(i) );
                alSourcef(source.get(i), AL_PITCH, t.pitch);
                alSourcef(source.get(i), AL_GAIN,  t.gain);
                alSource(source.get(i), AL_POSITION, sourcePos);
                alSource (source.get(i), AL_VELOCITY, sourceVel     );
                if(t.loop){
                    alSourcei(source.get(t.id), AL_LOOPING,  AL_TRUE  );
                }

                //System.out.println("Binding source " + t.path + ": " + alGetString(alGetError()));
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    public void setGain(Track t){
        alSourcef(source.get(t.id), AL_GAIN,  t.gain);
    }
    public void setPitch(Track t){
        alSourcef(source.get(t.id), AL_PITCH, t.pitch);
    }
}
