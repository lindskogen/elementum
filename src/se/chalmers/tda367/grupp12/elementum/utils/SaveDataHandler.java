package se.chalmers.tda367.grupp12.elementum.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Date: 2013-05-26
 * Time: 01:45
 */
public class SaveDataHandler extends Properties {

	private final static String LEVEL = "LEVEL";
	private final static String SCORE = "SCORE";

	private static SaveDataHandler instance;

	public static SaveDataHandler getInstance() {
		if (instance == null) {
			Properties def = new Properties();
			def.setProperty(LEVEL, "");
			def.setProperty(SCORE, "0");
			instance = new SaveDataHandler(def);
		}
		return instance;
	}

	private SaveDataHandler(Properties defaultValues) {
		super(defaultValues);
		try {
			load(FileManager.getInstance().getSaveFile());
		} catch (IOException e) {
			System.err.println("Save file not found, using defaults...");
		}

	}

	public void reset() {
		setProperty(LEVEL, "");
		setProperty(SCORE, defaults.getProperty(SCORE));
	}

	public String getLevel() {
		return getProperty(LEVEL);
	}
	public int getScore() {
		return Integer.parseInt(getProperty(SCORE));
	}
	public void setLevel(String level) {
		setProperty(LEVEL, level);
	}
	public void setScore(int score) {
		setProperty(SCORE, "" + score);
	}
	public boolean saveProperties() {
		try {
			store(FileManager.getInstance().writeSaveFile(), null);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
