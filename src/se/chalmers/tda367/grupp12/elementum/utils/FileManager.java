package se.chalmers.tda367.grupp12.elementum.utils;
// STOP

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 4/17/13
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileManager {

	private static FileManager fileManager;
	private final Map<String, File> directories = new HashMap<String, File>();

	private FileManager(){
		makeTree("resources");
		generateFileList();
	}

	public static FileManager getInstance(){
		if(fileManager == null){
			fileManager = new FileManager();
		}
		return fileManager;
	}

	// Categorize once to speed up iteration
    void generateFileList(){
	}

	// Returns all files in a directory, if empty returns null
	private File[] getFilesInDirectory(String directoryPath){
		try{
			File directory = new File(directoryPath);
			return directory.listFiles();
		}catch(NullPointerException e){

		}
		return null;
	}

	private void makeTree(String inDir){
		try{
			File[] thisDir = getFilesInDirectory(inDir);
			for (File file : thisDir) {
				if(file.isDirectory()){
					//System.out.println(file.getPath());
					directories.put(file.getName(),file);
					if(hasSubDirectories(getFilesInDirectory(file.getPath()))){
						makeTree(file.getPath());
					}
				}
			}
		}catch(NullPointerException e){}
	}

	private boolean hasSubDirectories(File[] directory){

		for (File file : directory) {
			if(file.isDirectory()){
				return true;
			}
		}
		return false;
	}

	private File findFileInDirectory(String fileName, String directoryName) throws NullPointerException{
		File[] dir = new File(directories.get(directoryName).getPath()).listFiles();
		for (File file : dir ) {
			if(file.getName().equals(fileName)){
				return file;
			}
		}
        throw new NullPointerException("no such file");
	}

	private Image createImage(String image, String imageDirectory) throws SlickException {
		return new Image(findFileInDirectory(image, imageDirectory).getPath()).getFlippedCopy(false,true);
	}

	private Object getObject(File file) throws IOException, ClassNotFoundException {
		ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(file));
		return objIn.readObject();
	}

	public String getSoundEffect(String soundEffect){
		return findFileInDirectory(soundEffect, "soundeffects").getPath();
	}

	public Object getSong(String song) throws IOException, ClassNotFoundException {
		return getObject(findFileInDirectory(song, "songs"));
	}

	// N.S.I.N. (Not Sure If Necessary)
	public Object getOtherFile(String file) throws IOException, ClassNotFoundException {
		return getObject(findFileInDirectory(file, "other"));
	}

	public Image getSprite(String spriteName) throws SlickException {
		return createImage(spriteName, "sprites");
	}

	public Image getTexture(String texture) throws SlickException {
		return createImage(texture, "textures");
	}

	public Image getImage(String image) throws SlickException {
		return createImage(image, "images");
	}

	public void writeObject(String fileName, String directory, Object o) throws IOException {
		String path = directories.get(directory).getPath()+"/";
		ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(path+fileName));
		writer.writeObject(o);

		makeTree("resources");
	}

	public List<?> getLevelPolygons(String levelName){
		File dir = directories.get("levels");
		Object list = null;
		try {
			//System.out.println(dir.getPath() + "/" + levelName + "/entities.dat");
			list = new ObjectInputStream(new FileInputStream(dir.getPath() + "/" + levelName + "/entities.dat")).readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return (list instanceof List<?>) ? (List<?>) list : null;
	}

	public void writeLevelPolygons(String levelName, List<?> list) {
		File dir = directories.get("levels");
		try {
			ObjectOutputStream outstream = new ObjectOutputStream(new FileOutputStream(dir.getPath() + "/" + levelName + "/entities.dat"));
			outstream.writeObject(list);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public InputStream getSaveFile() throws FileNotFoundException {
		return new FileInputStream("savefile.properties");
	}
	public OutputStream writeSaveFile() {
		try {
			return new FileOutputStream("savefile.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public BufferedWriter getLevelWriter(String levelName){
		File dir = directories.get("levels");
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(dir.getPath()+"/"+levelName + "/level.lvl"));
		} catch (IOException e) {
			System.err.println("Failed to save level " + levelName);

			try {
				File tmp = new File(dir.getPath()+"/"+levelName);
				tmp.mkdirs();
				writer = new BufferedWriter(new FileWriter(tmp.getPath()+"/level.lvl"));
			}catch(IOException e2){
				System.err.println("failed to create level folder");
			}
		}

		makeTree("resources");
		return writer;
	}

   public List<String> getLevels(){
       List<String> levels = new ArrayList<String>();
       File[] files = directories.get("levels").listFiles();
       for(File f:files){
           if(f.isDirectory()){
               levels.add(f.getName());
           }
       }
       return levels;
   }

	public BufferedReader readLevel(String levelName) throws FileNotFoundException {
		File dir = directories.get(levelName);

		if(dir == null){
			throw new FileNotFoundException();
		}

		return new BufferedReader(new FileReader(dir.getPath() + "/level.lvl"));
	}
}
