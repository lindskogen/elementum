package se.chalmers.tda367.grupp12.elementum.controller;

import org.jbox2d.common.Vec2;

import java.util.Map;

/**
 * This is an interface describing an object which should act as a receiver for the controller. By implementing these
 * methods they will be run by the controller if the receiver is set and the corresponding hotkey is pressed.
 * Date: 2013-04-15
 * Time: 12:14
 */
public interface InputAction {
    public void mouse(Vec2 pos);
	public void up();
	public void down();
	public void left();
	public void right();
	public void enter();
	public void pause();
	public void hotkey(int id);
    public Map<KeyConfig.Action,Boolean> getRepeat();
}
