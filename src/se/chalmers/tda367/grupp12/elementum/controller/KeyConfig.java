package se.chalmers.tda367.grupp12.elementum.controller;

import org.lwjgl.input.Keyboard;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This class binds specific actions with specific hotkeys. It also takes car of if the keys shall support repeat or no.
 * Date: 2013-04-11
 * Time: 17:51
 */
public class KeyConfig {
	private static Map<Integer, Action> keys;
	private final static String KEYCONFIG = "keyconfig.dat";

	public enum Action {        //All the actions
		UP(true), DOWN(true), LEFT(true), RIGHT(true), ENTER, PAUSE,
        HOT0,HOT1, HOT2, HOT3, HOT4,HOT5,HOT6,HOT7,HOT8,HOT9,HOT10R(true),HOT11R(false),HOT12R(false),HOT13R(false),HOT14R(false),
        HOT15,HOT16,HOT17,HOT18,HOT19,HOT20;
		private final boolean repeats;
		Action() {
			repeats = false;
		}
		Action(boolean rep) {
			repeats = rep;
		}
		public boolean getRepeats() {
			return repeats;
		}
	}

    /**
     * Attempts to load keyconfig file if there is such a file
     */
	private static void readConfig() {
		try {
			// TODO use FileUtils from Patrik instead of ObjectInputStream (dependency: FileManager-branch)
			ObjectInputStream oistream = new ObjectInputStream(new FileInputStream(KEYCONFIG));
			Object obj = oistream.readObject();
			keys = (Map<Integer, Action>) obj;
			oistream.close();
		} catch (IOException e) {
			System.err.println("Keyconfig file not found, using defaults");
			setDefaults();
		} catch (ClassNotFoundException e) {
			setDefaults();
			throw new IllegalStateException("Unable to cast keyconfig Map");
		}
	}

    /**
     * saves the key configuration in a keyconfig file
     */
	private static void saveConfig() {
		try {
			ObjectOutputStream oostream = new ObjectOutputStream(new FileOutputStream(KEYCONFIG));
			oostream.writeObject(keys);
			oostream.close();
		} catch (IOException e) {
			System.err.println("Unable to write keyconfig to file");
			e.printStackTrace();
		}
	}

    /**
     * Binds keypresses to actions
     */
	private static void setDefaults() {
		if (keys == null) {
			keys = new HashMap<Integer, Action>();
		}

		keys.put(Keyboard.KEY_W, Action.UP);
		keys.put(Keyboard.KEY_SPACE, Action.UP);
		keys.put(Keyboard.KEY_A, Action.LEFT);
		keys.put(Keyboard.KEY_D, Action.RIGHT);
		keys.put(Keyboard.KEY_S, Action.DOWN);
		keys.put(Keyboard.KEY_RETURN, Action.ENTER);
		keys.put(Keyboard.KEY_P, Action.PAUSE);
		keys.put(Keyboard.KEY_ESCAPE, Action.PAUSE);

        keys.put(Keyboard.KEY_0, Action.HOT0);
		keys.put(Keyboard.KEY_1, Action.HOT1);
		keys.put(Keyboard.KEY_2, Action.HOT2);
		keys.put(Keyboard.KEY_3, Action.HOT3);
		keys.put(Keyboard.KEY_4, Action.HOT4);
        keys.put(Keyboard.KEY_5, Action.HOT5);
        keys.put(Keyboard.KEY_6, Action.HOT6);
        keys.put(Keyboard.KEY_7, Action.HOT7);
        keys.put(Keyboard.KEY_8, Action.HOT8);
        keys.put(Keyboard.KEY_9, Action.HOT9);
        keys.put(Keyboard.KEY_R, Action.HOT10R);
        keys.put(Keyboard.KEY_UP, Action.HOT11R);
        keys.put(Keyboard.KEY_DOWN, Action.HOT12R);
        keys.put(Keyboard.KEY_LEFT, Action.HOT13R);
        keys.put(Keyboard.KEY_RIGHT, Action.HOT14R);

        keys.put(Keyboard.KEY_N, Action.HOT16);
        keys.put(Keyboard.KEY_DELETE, Action.HOT17);
        keys.put(Keyboard.KEY_L, Action.HOT18);
        keys.put(Keyboard.KEY_K, Action.HOT19);
        keys.put(Keyboard.KEY_I, Action.HOT20);
		//saveConfig();
	}

    /**
     * converts an integer to the coresponding key
     * @param keyCode the Integer to look uo
     * @return  the key
     */
	public static Action getKeyAction(Integer keyCode) {
		if (keys == null) {
			readConfig();
		}
		return keys.get(keyCode);
	}
}
