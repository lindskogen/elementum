package se.chalmers.tda367.grupp12.elementum.controller;


import org.jbox2d.common.Vec2;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluUnProject;

/**
 * The main controller taking care of all input
 */
public class MainController {

	private InputAction currentReceiver;    //The current reciever
	private final KeyConfig.Action[] actions = new KeyConfig.Action[2];
    private Map<KeyConfig.Action,Boolean> repeat = null;

	public MainController() {}

	public void setCurrentReceiver(InputAction inputAction) {
        if(inputAction!=null){
            currentReceiver = inputAction;
            repeat = currentReceiver.getRepeat();
        }
	}

    /**
     * Runs through the eventstack and updates the current receiver
     */
	public void update() {
        if(currentReceiver!=null){
            currentReceiver.mouse(getMousePosition(Mouse.getX(), Display.getHeight()-Mouse.getY())); //Updates mouse position

            while (Keyboard.next()) {
                Integer key = Keyboard.getEventKey();

                // If event is key pressed set action to key action and trigger the event once,
                // this is to prevent any keypress to not
                for (int i = 0; i < actions.length; i++) {
                    if (Keyboard.getEventKeyState() && actions[i] == null) {
                        actions[i] = KeyConfig.getKeyAction(key);
                        break;
                    } else if (actions[i] == KeyConfig.getKeyAction(key) && !Keyboard.getEventKeyState()) {
                        actions[i] = null;
                    }
                }
            }
            for (int i = 0; i < actions.length; i++) {
                // If action is set the event should be triggered every tick
                if (actions[i] != null) {
                    doEvent(actions[i]);
                    //checks if it should do the event more than once
                    if(repeat!=null && repeat.get(actions[i])!=null){
                        actions[i] = repeat.get(actions[i])?actions[i]:null;
                    } else {
                        actions[i] = actions[i].getRepeats() ? actions[i] : null;
                    }
                }
            }
        }
	}

    /**
     * Depending on actions, runs corresponding methods in the current receiver
     * @param action specifies which method to run
     */
	private void doEvent(KeyConfig.Action action) {
		switch (action) {
			case UP:
				currentReceiver.up();
				break;
			case DOWN:
				currentReceiver.down();
				break;
			case LEFT:
				currentReceiver.left();
				break;
			case RIGHT:
				currentReceiver.right();
				break;
			case ENTER:
				currentReceiver.enter();
				break;
			case PAUSE:
				currentReceiver.pause();
				break;
            case HOT0:
                currentReceiver.hotkey(0);
                break;
			case HOT1:
				currentReceiver.hotkey(1);
				break;
			case HOT2:
				currentReceiver.hotkey(2);
				break;
			case HOT3:
				currentReceiver.hotkey(3);
				break;
			case HOT4:
				currentReceiver.hotkey(4);
				break;
            case HOT5:
                currentReceiver.hotkey(5);
                break;
            case HOT6:
                currentReceiver.hotkey(6);
                break;
            case HOT7:
                currentReceiver.hotkey(7);
                break;
            case HOT8:
                currentReceiver.hotkey(8);
                break;
            case HOT9:
                currentReceiver.hotkey(9);
                break;
            case HOT10R:
                currentReceiver.hotkey(10);
                break;
            case HOT11R:
                currentReceiver.hotkey(11);
                break;
            case HOT12R:
                currentReceiver.hotkey(12);
                break;
            case HOT13R:
                currentReceiver.hotkey(13);
                break;
            case HOT14R:
                currentReceiver.hotkey(14);
                break;
            case HOT15:
                currentReceiver.hotkey(15);
                break;
            case HOT16:
                currentReceiver.hotkey(16);
                break;
            case HOT17:
                currentReceiver.hotkey(17);
                break;
            case HOT18:
                currentReceiver.hotkey(18);
                break;
            case HOT19:
                currentReceiver.hotkey(19);
                break;
            case HOT20:
                currentReceiver.hotkey(20);
                break;
		}
	}

    /**
     * Converter between window position pixels and gl position, used to get mouse position
     * @param mouseX xpos
     * @param mouseY ypos
     * @return
     */
    private static Vec2 getMousePosition(int mouseX, int mouseY) {

        /* Draw depth support quad in order to get correct position*/
        glPushMatrix();
        glTranslatef(0, 0, 0);
        int s = 1000;
        glBegin(GL_QUADS);        // Draw The Cube Using quads
        glVertex2f(s/2, s/2);    // Top Right Of The Quad (Front)
        glVertex2f(-s/2, s/2);    // Top Left Of The Quad (Front)
        glVertex2f(-s/2, -s/2);    // Bottom Left Of The Quad (Front)
        glVertex2f(s / 2, -s / 2);    // Bottom Right Of The Quad (Front)
        glEnd();
        glPopMatrix();

        IntBuffer viewport = BufferUtils.createIntBuffer(16);
        FloatBuffer modelview = BufferUtils.createFloatBuffer(16);
        FloatBuffer projection = BufferUtils.createFloatBuffer(16);
        FloatBuffer winZ = BufferUtils.createFloatBuffer(1);
        float winX, winY;
        FloatBuffer position = BufferUtils.createFloatBuffer(3);
        glGetFloat(GL_MODELVIEW_MATRIX, modelview);
        glGetFloat(GL_PROJECTION_MATRIX, projection);
        glGetInteger( GL_VIEWPORT, viewport );
        winX = (float)mouseX;
        winY = (float)viewport.get(3) - (float)mouseY;

        glReadPixels(mouseX, (int)winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, winZ);
        //winZ.get()
        gluUnProject(winX, winY, winZ.get(), modelview, projection, viewport, position);
        //System.out.println(position.get(0)+" "+position.get(1));
        return new Vec2(position.get(0),position.get(1));
    }

}
