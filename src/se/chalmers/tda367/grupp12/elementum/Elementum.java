package se.chalmers.tda367.grupp12.elementum;

import org.jbox2d.common.Settings;
import org.lwjgl.opengl.Display;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.MainController;
import se.chalmers.tda367.grupp12.elementum.model.*;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;
import se.chalmers.tda367.grupp12.elementum.utils.SaveDataHandler;
import se.chalmers.tda367.grupp12.elementum.view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 4/12/13
 * Time: 2:36 PM
 */
public class Elementum implements ActionListener{
	public static final String MODEL ="Change model";
    public static final String QUIT ="Quit";
	public static final String LISTENER ="Change listener";

    private static final int MAXVERTICES = 100;
	private boolean manualClose = false;
    private List<Model> models;
    private View view;
    private Model activeModel;
    private MainController controller;
    public void start(){
        Settings.maxPolygonVertices = MAXVERTICES; //Max vertex count
        try {
            view = new View(false);
            controller = new MainController();
            models = new ArrayList<Model>();
            models.add(new LoadingScreen());
            models.add(new Title(this));
            models.add(new Universe(this));
            models.add(new LevelSelectMenu(this,false));
            models.add(new LevelSelectMenu(this,true));
            models.add(new InGameMenu(this));
            models.add(new LevelEditor(this));
            models.add(new GameClearedScreen(this));
            this.actionPerformed(new ActionEvent("", 1, Elementum.MODEL));


            while(!Display.isCloseRequested() && !manualClose){
                activeModel.update();
                view.update();
	            controller.update();
                FPSManager.getSharedInstance().update();
            }


        } catch (Exception e) {
	        System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
	    // If save succeeded: exit 0, else exit 1.
	    System.exit(SaveDataHandler.getInstance().saveProperties() ? 0 : 1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //System.out.println(e.getSource().getClass()); //debug
	    if(e.getActionCommand().equals(Elementum.MODEL)&&e.getSource() instanceof String){
            Audio.Track oldt = null;
            if(activeModel !=null)
                oldt= activeModel.getSong();

            try{
                activeModel = models.get(e.getID());
            }catch(IndexOutOfBoundsException exception){
                return;
            }
	        activeModel.reload((String)e.getSource());
	        view.setModel(activeModel);
	        controller.setCurrentReceiver(activeModel.getInputAction());

            // In case of loadingscreen we forceupdate the view, only solution since update of view is not in separate thread
            if(activeModel instanceof LoadingScreen) try {
                view.update();
            } catch (View.ModelNotSetException e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            Audio.Track t = activeModel.getSong();
            if(oldt==null||t!=oldt){
                if(oldt!=null)
                    Audio.getSharedInstance().stop(oldt);
                if(t!=null)
                    Audio.getSharedInstance().play(t);
            }

        } else if(e.getActionCommand().equals(Elementum.LISTENER)){
		    if(e.getSource() instanceof InputAction){
			    controller.setCurrentReceiver((InputAction) e.getSource());
		    }
	    } else if(e.getActionCommand().equals(Elementum.QUIT)){
            manualClose = true;
        }
    }
}
