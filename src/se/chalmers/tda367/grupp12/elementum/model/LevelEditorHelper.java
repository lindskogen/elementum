package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-04-15
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */
public class LevelEditorHelper extends Entity {
    public LevelEditorHelper(Vec2 pos){
        super(new World(new Vec2(0,0),true), BodyType.STATIC, pos);
        setz(Entity.PRIORITY_8);
        createFixtureDef();
    }
    public LevelEditorHelper(Vec2 pos, Shape shape){
        super(new World(new Vec2(0,0),true),BodyType.STATIC, pos);
        setz(Entity.PRIORITY_8);
        createFixtureDef(shape);
    }

    public void update() {}

    void createFixtureDef(){
        FixtureDef fd = new FixtureDef();
        fd.density = 1f;
        fd.shape=Entity.makeCircle(0.3f,20);
        fd.friction = 1f;
        body.createFixture(fd);
    }
    public void createFixtureDef(FixtureDef def){
        if(body.getFixtureList() !=null)
            body.destroyFixture(body.getFixtureList());
        getBody().createFixture(def);
    }
    void createFixtureDef(org.jbox2d.collision.shapes.Shape shape){
        FixtureDef fd = new FixtureDef();
        fd.density = 1f;
        fd.shape=shape;
        fd.friction = 1f;
        if(body.getFixtureList() !=null)
            body.destroyFixture(body.getFixtureList());

        body.createFixture(fd);
    }
}
