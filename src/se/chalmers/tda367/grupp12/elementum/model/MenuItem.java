package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Menu class with unfinished implementation, not in use
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 4/23/13
 * Time: 3:49 PM
 */
public class MenuItem extends Drawable {
	private String name;
	private final Image unselectedImage;
	private final Image selectedImage;

	private ActionListener l;

	private final Vec3 position = new Vec3(0,0,Drawable.PRIORITY_8);
	private final ContentImage image;
	private boolean active;
    private List<Vec2> vecList = new ArrayList<Vec2>();

	public MenuItem(String name, String fileName, String selectionImage){
		active = false;
		unselectedImage = setImage(fileName);
		selectedImage = setImage(selectionImage);
		image = new ContentImage(name, unselectedImage);

        vecList.add(new Vec2(0,(float)image.getImage().getHeight()));
        vecList.add(new Vec2(0,0));
        vecList.add(new Vec2((float)image.getImage().getWidth(),(float)image.getImage().getHeight()));
        vecList.add(new Vec2((float)image.getImage().getWidth(),0));
	}

	private Image setImage(String fileName){
		try {
			return FileManager.getInstance().getImage(fileName);
		} catch (SlickException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		return null;
	}

	public void setPosition(float x, float y){
		position.x = x;
		position.y = y;
	}

	public String getName(){
		return name;
	}

	public void setActive(Boolean active){
		this.active = active;
		if(active){
			image.setImage(selectedImage);
		}else{
			image.setImage(unselectedImage);
		}
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public List<Vec2> getVectors() {
		return vecList;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Image getTexture() {
		return active?selectedImage:unselectedImage;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public boolean isRepeatedTextures() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public float getDepth() {
		return 0;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public boolean isCreative() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Vec3 getPosition() {
		return position;
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public Color getColor() {
		return null;
	}
}
