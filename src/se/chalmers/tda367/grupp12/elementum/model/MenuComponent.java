package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.KeyConfig;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a menu component class which represents a menu
 * Date: 5/24/13
 * Time: 10:54 AM
 */
public class MenuComponent implements InputAction {
    private int indexDemand = 0;
    private float margin = 0.05f;
    private ActionListener l;
    private float index;
    private List<MenuAction> menuAction;
    private Animation a;
    private Vec3 pos;
    private float widths;

    public MenuComponent(ActionListener l, int startIndex, float width, Vec3 pos, List<MenuAction> menuAction, Animation a, int charcount){
        this.l=l;
        this.index=startIndex;
        this.menuAction=menuAction;
        this.pos=pos;
        widths=width/charcount;
        if(a!=null)this.a=a;
    }

    /**
     * Updates the index where we are at in the menu as well as "rolling" (animate) the menu
     */
    public void update(){
        if(a!=null)a.update();
        if(indexDemand==-1){
            index-= 3/FPSManager.getSharedInstance().getFPS();
        } else if(indexDemand==1){
            index+= 3/FPSManager.getSharedInstance().getFPS();
        }
        if(indexDemand==-1&&((index-margin)<Math.floor(index))){
            indexDemand = 0;
        } else if(indexDemand==1&&(index+margin)>Math.ceil(index)){
            indexDemand =0;
        }
        if(index<0)index=0;
        if(index>menuAction.size()-1)index=menuAction.size()-1;
    }

    /**
     * {@inheritDoc}
     * calculates all the drawables dependong on where we are in the menu
     * @return
     */
    public List<Drawable> getDrawables(){
        List<Drawable> d = new ArrayList<Drawable>();
        for(MenuAction s:menuAction){
            /*tons of complex calculations in order to propperly animate the menu*/
            float index = menuAction.indexOf(s);
            float distance = this.index-index;
            float absDistance = Math.abs(distance);
            float y = pos.y-widths*1.5f*index-0.5f*index+widths*1.5f*this.index+0.5f*this.index-distance*1;
            float z = pos.z-(float)Math.pow((absDistance*1f),1.2f);
            Color c = Math.round(this.index)==index?new Color(117/255f,105/255f,50/255f,1):new Color(1,1,1,1-(absDistance>3?3:absDistance)*0.3f);
            float xrot = (float)Math.pow(absDistance,1.5f)*-0.1f;
            if(distance<0)xrot=-xrot;
            d.addAll(FontManager.getSharedInstance().getString(
                    new Vec2(widths-absDistance*0.1f,(widths-absDistance*0.1f)*1.5f),
                    new Vec3(pos.x+absDistance*2,y,z),c,s.getS(),
                    new Vec3(xrot,0,0)));
        }
        if(a!=null){   // if there is an animation specified we add that at the current index
            a.setPos(new Vec3(pos.x-7,pos.y-widths*1.5f*index-0.5f*index+widths*1.5f*this.index+0.5f*this.index+widths/2,pos.z));
            a.setSize(new Vec2(5,5));
            d.add(a.getDrawable());
        }
        return d;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouse(Vec2 pos) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void up() {
        indexDemand=-1;
        index-=margin;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void down() {
        indexDemand=1;
        index+=margin;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void left() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void right() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enter() {
        l.actionPerformed(menuAction.get(Math.round(index)).getE());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pause() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hotkey(int id) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<KeyConfig.Action, Boolean> getRepeat() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void setIndex(int index) {
        if(index>=0&&index<menuAction.size()-1)
            this.index = index;
    }
}
