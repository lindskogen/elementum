package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 5/22/13
 * Time: 6:24 PM
 */
public class MenuModel implements Model, ActionListener {

    private List<Drawable> drawables = new ArrayList<Drawable>();
    private CameraInterface camera = new DummyCamera();
    private MenuComponent menu;
    private String title;

    public MenuModel(ActionListener l, List<MenuAction> menuActions, String imgPath, String title){
        this.title=title;
        Vec2 renderArea= Helpers.calcRenderArea().mul(camera.getCameraPosition().z);
        try {
            drawables.add(new DrawableImage(FileManager.getInstance().getImage(imgPath),new Vec2(renderArea.x,renderArea.y),
                    new Vec3(-renderArea.x/2,-renderArea.y/2,0),null,null));
        } catch (SlickException e) {
            e.printStackTrace();
        }
        renderArea= Helpers.calcRenderArea().mul(camera.getCameraPosition().z-20);
        menu = new MenuComponent(l,0,renderArea.x-70,new Vec3(-renderArea.x/2+5,renderArea.y/2-40,15),menuActions,null,15);
    }

    @Override
    public List<Drawable> getDrawables() {
        List<Drawable> d = new ArrayList<Drawable>();
        d.addAll(FontManager.getSharedInstance().getString(title.length()>10?new Vec2(6,9):new Vec2(12, 18), new Vec3(-55, 22, 10), Color.WHITE, title,null));
        d.addAll(drawables);
        d.addAll(menu.getDrawables());
        return d;
    }

    @Override
    public void update() {
        menu.update();
    }

    @Override
    public InputAction getInputAction() {
        return menu;
    }

    @Override
    public CameraInterface getCamera() {
        return camera;
    }

    @Override
    public void reload(String s) {
        menu.setIndex(0);
    }

    @Override
    public Audio.Track getSong() {
        return Audio.MAIN;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
    }

}
