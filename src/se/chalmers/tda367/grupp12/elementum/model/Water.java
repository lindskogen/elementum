package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Date: 2013-04-10
 * Time: 20:37
 */
public class Water extends Element {

    private static final float MAX_SPEED = 3f;
    private static final float ACCELERATION = 130;
    private Animation animation = null;

    public Water() {

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(1,0.7f,new Vec2(0,-0.6f), 0);
        fixdef.friction = 0f;
        fixdef.density = 1f;
        fixdef.shape = shape;

        try {
	        FileManager fm = FileManager.getInstance();
            Image sprite = fm.getSprite("newWater.png");
            SpriteSheet t = new SpriteSheet(sprite,512,159);     //Sets the current elements animation sprite
            animation = new Animation(t,50);
            //animation = new Animation(sprite,0,0,sprite.getHorizontalCount()-1,sprite.getVerticalCount()-1,true,500,false);

        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
	}


    @Override
    public void moveLeft(Player p) {
        Vec2 force = new Vec2(-ACCELERATION,0);
            p.getBody().applyForce(force, new Vec2(p.getBody().getPosition()));
    }

    @Override
    public void moveRight(Player p) {
        Vec2 force = new Vec2(ACCELERATION,0);
            p.getBody().applyForce(force, new Vec2(p.getBody().getPosition()));
    }

    @Override
    public void moveDown(Player p) {        //Can move up and down if those directions are available in Player
        if(p.getAvailableDirections()[3]&&!(p.getBody().getLinearVelocity().y < -MAX_SPEED)){
                Vec2 force = new Vec2(0,-ACCELERATION);
                p.getBody().applyForce(force, new Vec2(p.getBody().getPosition()));
        }
    }

    @Override
    public void moveUp(Player p) {
        if(p.getAvailableDirections()[2] && !(p.getBody().getLinearVelocity().y > MAX_SPEED)){
            Vec2 force = new Vec2(0,ACCELERATION);
            p.getBody().applyForce(force, new Vec2(p.getBody().getPosition()));
        }
    }

    @Override
    public void update(Player p) {
        Body b = p.getBody();
        Vec2 linvel = b.getLinearVelocity();
        if(linvel.x > MAX_SPEED || linvel.x < -MAX_SPEED)       //Stops the player from exceeding the max velocity
            b.setLinearVelocity(new Vec2(linvel.x>MAX_SPEED?MAX_SPEED:-MAX_SPEED,linvel.y));
        animation.update(FPSManager.getSharedInstance().getElapsedTimeMillis());
        p.setTexture(animation.getCurrentFrame());              //Updates the sprite-animation of the element
    }                                                           //by checking how many frames that have passed
}
