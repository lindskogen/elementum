package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.Image;
import se.chalmers.tda367.grupp12.elementum.model.Player;


/**
 * Date: 2013-04-10
 * Time: 18:04
 */
public abstract class Element {
	FixtureDef fixdef;

	public Element() {
		this(new FixtureDef());
	}
	private Element(FixtureDef def) {
		fixdef = def;
	}
    public FixtureDef getFixture(){
        return fixdef;
    }
	public PolygonShape getShape() {
		return null;
	}

    public Image getTexture(){
        return null;
    }

	public abstract void moveLeft(Player p);
	public abstract void moveRight(Player p);
	public abstract void moveDown(Player p);
	public abstract void moveUp(Player p);
	public abstract void update(Player p);
}
