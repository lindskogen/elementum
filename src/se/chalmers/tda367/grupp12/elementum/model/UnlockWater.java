package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-05-24
 * Time: 14:56
 * To change this template use File | Settings | File Templates.
 */
public class UnlockWater extends Collectable {

	private Player.Elements element;
	private Image texture;

	public UnlockWater(World w, Vec2 pos){
		super(w,pos);
		setRepeatedTextures(false);

		try {
			texture = FileManager.getInstance().getTexture("runtsten.png");
		} catch (SlickException e) {                    //Sets the texture of the element
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		FixtureDef fd = new FixtureDef();
		fd.shape = Entity.makeRectangle(0.5f,0.5f);
		fd.isSensor = true;
		body.createFixture(fd);
		reload();
	}

	public void interact(Player p){
		super.interact(p);
		boolean[] b = p.getAvailableElements();
		b[1] = true;
		p.setAvailableElements(b);
	}
}
