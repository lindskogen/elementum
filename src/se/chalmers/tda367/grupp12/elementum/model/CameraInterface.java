package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec3;

/**
 * Date: 4/17/13
 * Time: 1:37 PM
 * Interface for the camera.
 */
public interface CameraInterface {
    public void lookAt();
    public Vec3 getCameraPosition();
}
