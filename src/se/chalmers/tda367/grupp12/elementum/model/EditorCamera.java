package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec3;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.Project.gluLookAt;

/**
 * Date: 4/17/13
 * Time: 1:51 PM
 *
 * Camera specifically for the level editor
 */
public class EditorCamera implements CameraInterface {
    private final Vec3 cameraPosition = new Vec3(0, 0, 20.0f);
    private final Vec3 cameraAim = new Vec3(0.0f, 0.0f, 0.0f);
    private final Vec3 cameraOrientation = new Vec3(0.0f, 1.0f, 0.0f);

	//Moves the camera a distance x, y
    public void move(float x, float y){
        cameraAim.x+=x;
        cameraAim.y+=y;
        cameraPosition.x+=x;
        cameraPosition.y+=y;
    }

	// Tilts the camera a deltaposition x, y
    public void tilt(float x, float y){
        cameraAim.x+=x;
        cameraAim.y+=y;
    }
    public void resetTilt(){
        cameraAim.x=cameraPosition.x;
        cameraAim.y=cameraPosition.y;
    }

	public Vec3 getTilt(){
		return new Vec3(cameraAim.x, cameraAim.y, cameraAim.z);
	}

    public void zoom(float z){
        cameraPosition.z+=z;
    }
    public void lookAt(){
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(cameraPosition.x,cameraPosition.y,cameraPosition.z,
                cameraAim.x,cameraAim.y,cameraAim.z,
                cameraOrientation.x,cameraOrientation.y,cameraOrientation.z);
    }

    @Override
    public Vec3 getCameraPosition() {
        return new Vec3(cameraPosition.x,cameraPosition.y,cameraPosition.z);
    }
}
