package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.lwjgl.opengl.Display;
import se.chalmers.tda367.grupp12.elementum.model.Polygon2D;

import java.awt.geom.Area;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Date: 4/23/13
 * Time: 1:38 PM
 */
public class Helpers {
    /*helper methods*/
	//applies rotation matrix to the given vector
    public static Vec2 rotate(Vec2 v,float rot){
        return new Vec2(v.x*(float)Math.cos(rot)-v.y*(float)Math.sin(rot),
                v.x*(float)Math.sin(rot)+v.y*(float)Math.cos(rot));
    }
    /*Distance*/
    public static float distance(Vec2 v1, Vec2 v2){
        return (float)Math.sqrt(Math.pow(v2.x-v1.x,2) + Math.pow(v2.y-v1.y,2));
    }
    /*Angle*/
    public static float angle(Vec2 v1, Vec2 v2){
        return (float) Math.toDegrees(Math.acos(Vec2.dot(v1,v2)/(v1.length()*v2.length())));
    }

    public static boolean isConvexPolygon(List<Vec2> p){
	    if(p == null || p.size() <=2){
		    return false;
	    }

	    p.add(p.get(0));
        for(Vec2 v:p){
            if( angle (v, p.get(p.indexOf(v)+1)) > 180) return  false;
        }
        p.remove(p.size()-1);
        return true;
    }
    public static float area(Area a){
        if(a.isPolygonal()){
            Polygon2D polygon = new Polygon2D();
            PathIterator pi = a.getPathIterator(null);
            while(!pi.isDone()){
                float point[] = new float[2];
                pi.currentSegment(point);
                polygon.addPoint(point[0],point[1]);
                pi.next();
            }
            polygon.addPoint(polygon.xpoints[0],polygon.ypoints[0]); //closing it
            float sum = 0;
            int n =polygon.npoints;
            float x[] = polygon.xpoints;
            float y[] = polygon.ypoints;
            for (int i = 0; i < n -1; i++)
            {
                sum = sum + x[i]*y[i+1] - y[i]*x[i+1];
            }
            sum/=2;
            return Math.abs(sum);
        } else {
            throw new IllegalArgumentException("not a polygon");
        }
    }

    public static double getIntersectArea(Rectangle2D a,Rectangle2D b){
        Rectangle2D r = new Rectangle2D.Double();
        Rectangle2D.intersect(a, b, r);
        return  rectArea(r);
    }

    private static double rectArea(Rectangle2D a){
        return a.getWidth() * a.getHeight();
    }

    public static Vec2 calcRenderArea() {
        float balancer = 2*(float)Math.tan(Math.toRadians(100/2));
        Vec2 v= new Vec2(balancer*Display.getWidth()/ Display.getHeight(),balancer);
        return v;
    }
}
