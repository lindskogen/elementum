package se.chalmers.tda367.grupp12.elementum.model;


import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.lwjgl.opengl.Display;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.Project.gluLookAt;

/**
 * Camera when in game mode, this camera follows the player
 */
public class PlayerCamera implements CameraInterface{

    private final Vec3 cameraPosition = new Vec3(0.0f, 0.0f, 8.0f);
    private final Vec3 cameraAim = new Vec3(0.0f, 0.0f, 0.0f);
    private final Vec3 cameraOrientation = new Vec3(0.0f, 1.0f, 0.0f);

    private Player player;

	/*
		To keep the game experience more fluid, the camera only moves when it has to, the player therefore
		has a span to move in not followed by the camera. The margin of this span is here defined as playerMargin,
		and tells us the movable space the player has on screen before the camera starts
		to follow the player
		0.4 is a percentage of half the screen, this is then converted from pixels to gl squares by divsion by 60.
	*/
	private final double playerMargin = 0.4/60;
	private final float xDistance = (float)(Display.getWidth() * playerMargin);
	private float yDistance = (float)(Display.getHeight() * playerMargin);

    public PlayerCamera(Player player){
        this.player=player;
        cameraPosition.x=cameraAim.x=player.getBody().getPosition().x;
        cameraPosition.y = cameraAim.y = player.getBody().getPosition().y+3;
    }

	// Set the player to follow
    public void setPlayer(Player p){
        this.player=p;
    }

	// Update the camera to follow the player
    public void updateCamera(){
		Vec2 p = player.getBody().getPosition();
	    if(cameraPosition.x - p.x < -xDistance){
		    cameraPosition.x = cameraAim.x = p.x - xDistance;
	    }else if (cameraPosition.x - p.x >  xDistance){
		    cameraPosition.x = cameraAim.x = p.x + xDistance;
	    }
        cameraPosition.y = cameraAim.y = p.y+3;
    }
    public void lookAt(){
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(cameraPosition.x,cameraPosition.y,cameraPosition.z,
                cameraAim.x,cameraAim.y,cameraAim.z,
                cameraOrientation.x,cameraOrientation.y,cameraOrientation.z);
    }

    @Override
    public Vec3 getCameraPosition() {
        return new Vec3(cameraPosition.x,cameraPosition.y,cameraPosition.z);
    }

}
