package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-05-24
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class CollectableEarth extends Collectable{

	public CollectableEarth(World w, Vec2 pos){
		super(w,pos);

		try {
            setTexture(FileManager.getInstance().getTexture("earthsymbol.png"));
		} catch (SlickException e) {                    //Sets the texture of the element
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}


	@Override
	public void interact(Player p) {
		if(p.getState() == Player.Elements.EARTH){
			super.interact(p);
			p.thingCollected();
		}
	}




}
