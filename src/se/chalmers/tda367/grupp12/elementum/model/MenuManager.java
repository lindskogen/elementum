package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.KeyConfig;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 4/23/13
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class MenuManager implements InputAction{
	private static MenuManager menuManager;
	private int currentMenuItem;
	private ArrayList<MenuItem> menuItems;
	private boolean active;

	private ActionListener l;

	// Make a list with the menuitems you want, then make a new menu as MenuManager MainMenu = ..
	private MenuManager(){
		currentMenuItem = 0;
		active = false;
	}

	public static MenuManager getInstance(){
		if(menuManager == null){
			menuManager = new MenuManager();
		}
		return menuManager;
	}

	public void setMenu(ArrayList<MenuItem> menuItems){
		this.menuItems = menuItems;
		menuItems.get(currentMenuItem).setActive(true);
		positionMenuItems();
	}

	void previousMenuItem(){
		menuItems.get(currentMenuItem).setActive(false);
		if(currentMenuItem == 0){
			currentMenuItem = menuItems.size();
		}
		currentMenuItem = (currentMenuItem-1);
		menuItems.get(currentMenuItem).setActive(true);
	}

	void nextMenuItem(){
		menuItems.get(currentMenuItem).setActive(false);
		currentMenuItem = (currentMenuItem+1)%(menuItems.size()-1);
		menuItems.get(currentMenuItem).setActive(true);
	}

	public void enterMenuItem(){
		menuItems.get(currentMenuItem).getName();
	}

	public boolean isActive(){
		return active;
	}

	public void toggleMenu(){
		active = !active;
	}

	void positionMenuItems(){
		float yPos = 5f;
		for(MenuItem m : menuItems){
			m.setPosition(-2.7f,yPos);
			yPos--;
		}
	}

	@Override
	public void mouse(Vec2 pos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void up() {
		previousMenuItem();
	}

	@Override
	public void down() {
		nextMenuItem();
	}

	@Override
	public void left() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void right() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void enter() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void pause() {
		l.actionPerformed(new ActionEvent(this,0, Universe.MENU));
		//toggleMenu();
	}

	@Override
	public void hotkey(int id) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

    @Override
    public Map<KeyConfig.Action, Boolean> getRepeat() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setActionListener(ActionListener l) {
		this.l=l;
	}

	public Collection<? extends Drawable> getDrawables() {
		return menuItems;
	}
}
