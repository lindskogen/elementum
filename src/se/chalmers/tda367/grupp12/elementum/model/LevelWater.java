package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;

/**
 * Date: 2013-04-10
 * Time: 20:58
 */

public class LevelWater extends Entity implements Interactable {
    private boolean active = false;

	public LevelWater(World w,Vec2 pos){
		super(w, BodyType.STATIC, pos);
        body.setUserData(this);
        setDepth(0);
        setz(PRIORITY_3);
        setColor(new Color(1,1,1,0.5f));
        try {
            setTexture(FileManager.getInstance().getTexture("water.png"));
        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
		FixtureDef fixture = new FixtureDef();
		fixture.shape = makeRectangle(1,1);
		fixture.isSensor = true;
		body.createFixture(fixture);
    }

	@Override
	public void update() {

	}
	public void interact(Player p){
        boolean[]b=p.getAvailableElements();
        b[2]=false;
        p.setAvailableElements(b);
        if(p.getState() == Player.Elements.WATER){
            boolean d[]={true,true,true,true};
            p.setAvailableDirections(d);
        }else if(p.getState() == Player.Elements.WIND && !getTransformedAndRotatedPolygon().getBounds2D().contains(p.getTransformedAndRotatedPolygon().getBounds2D())){
            if(p.getBody().getLinearVelocity().y<5)
                p.getBody().setLinearVelocity(new Vec2(p.getBody().getLinearVelocity().x,0.7f));
        } else if (p.getState() == Player.Elements.FIRE) {
            p.switchElement(Player.Elements.EARTH);
        }
            double ia = Helpers.getIntersectArea(p.getTransformedAndRotatedPolygon().getBounds2D(), getTransformedAndRotatedPolygon().getBounds2D());
            Vec2 wForce = new Vec2(0, (float) (-body.getWorld().getGravity().y * ia));
            //Vec2 wForce = new Vec2(0,-body.getWorld().getGravity().y * (p.getBody().getMass()/p.getBody().getFixtureList().getDensity()));
			p.getBody().applyForce(wForce, p.getBody().getPosition());
			p.getBody().setLinearVelocity(new Vec2(p.getBody().getLinearVelocity().x * 0.995f,p.getBody().getLinearVelocity().y*0.995f));
	}

    @Override
    public void reset(Player p) {
        boolean[]b=p.getAvailableElements();
        b[2]=true;
        p.setAvailableElements(b);
        if(p.getState() == Player.Elements.WATER){
            boolean d[]={true,true,false,false};
            p.setAvailableDirections(d);
        }
    }

    @Override
    public void setActive(boolean b) {
        this.active=b;
    }

    @Override
    public boolean isActive() {
        return active;
    }
}
