package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.KeyConfig;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A dummy loadingscreen
 * Date: 5/24/13
 * Time: 11:58 PM
 */
public class LoadingScreen implements Model{
    private List<Drawable> drawables = new ArrayList<Drawable>();
    private CameraInterface camera = new DummyCamera();
    public LoadingScreen(){
        Vec2 renderArea= Helpers.calcRenderArea().mul(camera.getCameraPosition().z);
        try {
            drawables.add(new DrawableImage(FileManager.getInstance().getImage("title2.jpg"),new Vec2(renderArea.x,renderArea.y),
                    new Vec3(-renderArea.x/2,-renderArea.y/2,0),null,null));
        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        drawables.addAll(FontManager.getSharedInstance().getString(new Vec2(12, 18), new Vec3(-55, -6, 10), Color.WHITE, "loading",null));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Drawable> getDrawables() {
        return drawables;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputAction getInputAction() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CameraInterface getCamera() {
        return camera;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload(String s) {
    }


    @Override
    public Audio.Track getSong() {
        return null;
    }
}
