package se.chalmers.tda367.grupp12.elementum.model;

import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 5/22/13
 * Time: 6:24 PM
 */
public class InGameMenu extends MenuModel implements ActionListener {
    private static List<MenuAction> getMenuItems(){
        List<MenuAction> menuOptions = new ArrayList<MenuAction>();
        menuOptions.add(new MenuAction("Continue",new ActionEvent("",2,Elementum.MODEL)));
        menuOptions.add(new MenuAction("Main Menu",new ActionEvent("",1,Elementum.MODEL)));
        menuOptions.add(new MenuAction("Quit",new ActionEvent("",0,Elementum.QUIT)));
        return menuOptions;
    }
    private CameraInterface camera = new DummyCamera();

    public InGameMenu(ActionListener l){
        super(l,getMenuItems(),"title2.jpg","Game Menu");
    }

    @Override
    public List<Drawable> getDrawables() {
        List<Drawable> d = new ArrayList<Drawable>();
        d.addAll(super.getDrawables());
        return d;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public Audio.Track getSong() {
        return Audio.MAIN;
    }

}
