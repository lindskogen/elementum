package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-05-24
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public abstract class Collectable extends Entity implements Interactable, Destructible {
	private boolean active = false;
	private boolean collected = false;

	protected Collectable(World w, Vec2 pos) {
		super(w, BodyType.STATIC, pos);
		body.setUserData(this);
        setRepeatedTextures(false);
        try {
            setTexture(FileManager.getInstance().getTexture("collectible.png"));
        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        setDepth(0);
        FixtureDef fd = new FixtureDef();
        fd.shape= makeCircle(1, 50);
        fd.isSensor = true;
        createFixtureDef(fd);
        reload();
        setz(Drawable.PRIORITY_2);
    }

	public void interact(Player p) {
        Audio.getSharedInstance().play(Audio.COLLECT);
		body.destroyFixture(body.getFixtureList());
		collected = true;
	}

	public boolean destroyed(){
		return collected;
	}

	@Override
	public void update() {
        Vec3 oldrot = getRotation();
        setGRotation(new Vec2(oldrot.x,oldrot.y+2.5f/ FPSManager.getSharedInstance().getFPS()));
    }

	@Override
	public void reset(Player p) {}

	@Override
	public void setActive(boolean b) {
		this.active = b;
	}
	public boolean isActive() {
		return active;  //To change body of implemented methods use File | Settings | File Templates.
	}

}
