package se.chalmers.tda367.grupp12.elementum.model;


import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


public class LevelFactory {

	private static List<Polygon2D> levelPolygons;

	public static Level createLevel() {
		return createLevel("1.1");
	}

	public static Level createLevel(String name) {
		return parseLevel(name);
	}

	/*
	 * Text file looks like this:
	 * x,y - gravity
	 * x,y,x,y - player start and end position,
	 * followed by a list of entities.
	 * For a closer look, go to resources/levels/
	*/
	private static Level parseLevel(String levelName) {
		levelPolygons = (List<Polygon2D>)FileManager.getInstance().getLevelPolygons(levelName);
		Level lvl = null;
		BufferedReader reader = getLevelReader(levelName);

		try {
			Scanner sc = newScanner(reader.readLine());

			Vec2 gravity = new Vec2(sc.nextFloat(),sc.nextFloat());
			sc = newScanner(reader.readLine());
			Vec2 playerStart = new Vec2(sc.nextFloat(),sc.nextFloat());
			Vec2 playerEnd = new Vec2(sc.nextFloat(),sc.nextFloat());

			LevelProperties prop = new LevelProperties(gravity, null, playerStart, playerEnd);
			lvl = new Level(levelName,prop);

			while (reader.ready()) {
				lvl.addEntity(parseLine(lvl, reader.readLine()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lvl;
	}

	private static BufferedReader getLevelReader(String levelName){
		try {
			return FileManager.getInstance().readLevel(levelName);
		} catch (FileNotFoundException e) {
			try {
				return FileManager.getInstance().readLevel("1.1");
			} catch (FileNotFoundException exc) {
				return null;
			}
		}
	}

	private static Scanner newScanner(String string) {
		Scanner sc = new Scanner(string);
		sc.useLocale(Locale.US);
		sc.useDelimiter(",");
		return sc;
	}

	private static Entity parseLine(Level lvl, String line) {
		Scanner sc = new Scanner(line);
		sc.useDelimiter(",");
		sc.useLocale(Locale.US);

		String className = sc.next();
		Vec2 pos = new Vec2(sc.nextFloat(), sc.nextFloat());
		float rotation = sc.nextFloat();
		int entityIndex = sc.nextInt();

		Entity entity = createEntityObject(className, new Object[]{lvl, pos});

		Polygon2D polygon = levelPolygons.get(entityIndex);
		PolygonShape shape = new PolygonShape();
		Vec2[] polygons = new Vec2[polygon.npoints];
		for (int i = 0; i < polygon.npoints; i++) {
			polygons[i] = new Vec2(polygon.xpoints[i], polygon.ypoints[i]);
		}
		shape.set(polygons, polygon.npoints);

		if (shape == null) {
			throw new IllegalStateException("Shape was not set!");
		}

		entity.setShape(shape);

		entity.getBody().setTransform(entity.getBody().getPosition(), rotation);

		return entity;
	}

	private static Entity createEntityObject(String name, Object[] ctArgs) {
		String classPrefix = "se.chalmers.tda367.grupp12.elementum.model.";
		Object result = null;
		try {
			Class theClass = Class.forName(classPrefix + name);
			Class[] signature = new Class[]{World.class, Vec2.class};
			Constructor ct = theClass.getDeclaredConstructor(signature);
			result =  ct.newInstance(ctArgs);
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("Levelobject: " + name + " not supported.");
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return (Entity) result;
	}

	public static void saveLevel(Level lvl) {
		LevelProperties props = lvl.getLevelProperties();

		// First row of textfile is gravity: x,y
		StringBuilder builder = new StringBuilder();
		Vec2 gravity = props.getGravity();
		builder.append(gravity.x).append(",").append(gravity.y).append("\n");

		// Second row is startpos and endpos: x,y,x,y
		// TODO Player position
		Vec2 startPos = props.getStartPos();
		builder.append(startPos.x).append(",").append(startPos.y).append(",");

		Vec2 endPos = props.getEndPos();
		builder.append(endPos.x).append(",").append(endPos.y);


		// Set of polygons to be serialized into one file.
		// Appending the entities to the StringBuilder
		List<Entity> entities = lvl.getEntities();
		List<Polygon2D> polygons = new ArrayList<Polygon2D>(entities.size());

		BufferedWriter writer = FileManager.getInstance().getLevelWriter(lvl.getName());
		try {
			writer.write(builder.toString());
			writer.newLine();
			int entityIndex = 0;
			for (Entity ent : entities) {
				polygons.add(ent.getPolygon());
				writer.write(ent.toString() + "," + entityIndex++);
				writer.newLine();
			}
			writer.close();
		} catch (IOException e) {
			System.err.println("Write error: " + e.getMessage());
			e.printStackTrace();
		}
		FileManager.getInstance().writeLevelPolygons(lvl.getName(), polygons);
	}
}
