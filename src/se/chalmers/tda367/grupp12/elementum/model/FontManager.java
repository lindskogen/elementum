package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Date: 5/22/13
 * Time: 5:12 PM
 */
public class FontManager {
    private static final String s = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890:!. ");
    static private volatile FontManager instance = null;
    private HashMap<String,Image> textures = new HashMap<String, Image>();

    static public FontManager getSharedInstance() {
        if (instance == null) {
            instance = new FontManager();
        }
        return instance;
    }
    private FontManager(){
        try {
            Animation a = new Animation(new SpriteSheet(FileManager.getInstance().getSprite("font.png"),54,64),50);
            for(int i = 0;i<a.getFrameCount()&&i<s.length();i++){
                textures.put(String.valueOf(s.toCharArray()[i]),a.getImage(i));
            }

        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public List<Drawable> getString(Vec2 d, Vec3 p,Color c, String s,Vec3 rot){
        s=s.toUpperCase();
        List<Drawable> draw = new ArrayList<Drawable>();
        int index = 0;
        for(char ch: s.toCharArray()){
            draw.add(new DrawableImage(textures.get(String.valueOf(ch)),d,p.add(new Vec3(d.x*index,0,0)),c,rot));
            index++;
        }
        return draw;
    }
}
