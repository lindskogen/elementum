package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.contacts.ContactEdge;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.KeyConfig;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.SaveDataHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * Date: 2013-04-10
 * Time: 18:04
 */
public class Player extends Entity implements InputAction {
	/*A HashMap containing the sounds that are played for each element*/
	private final HashMap<Player.Elements,Audio.Track> elementTracks = new HashMap<Player.Elements, Audio.Track>();

	/*A boolean-array containing the directions that the player should be available to move in*/
    private boolean availableDirections[] = {true,true,false,false};

	/*A boolean-array containing the elements that the player should be able to switch to*/
	private boolean availableElements[] = {true,true,true,true};

	private final ActionListener l;

	/*An integer that determines if the player should be able to jump or not*/
	private static int jump;

	/*The players current element*/
	private Element currentElement;

	/*A enum-value of which element the player is currently in*/
	private Elements state;

	/*A HashMap containing the Elements (enum) connected to each Element*/
	private final HashMap<Elements,Element> elements = new HashMap<Elements, Element>();

	private int thingsCollected = SaveDataHandler.getInstance().getScore();

	/*An enum representing all the different elements*/
	public static enum Elements {
		WIND, WATER, FIRE, EARTH
	}

	public Player(World w, Vec2 pos, ActionListener l,Elements e) {
		this(w,pos,l);
		switchElement(e);
	}

	public Player(World w, Vec2 pos, ActionListener l) {        //Default constructor
		super(w, BodyType.DYNAMIC, pos);
		this.l=l;
		elementTracks.put(Player.Elements.FIRE, Audio.FIRE_ELEMENT);
		elementTracks.put(Player.Elements.WIND, Audio.WIND_ELEMENT);
		elementTracks.put(Player.Elements.WATER, Audio.WATER_ELEMENT);
		setz(Entity.PRIORITY_2);
		body.setUserData(this);
		elements.put(Elements.WIND, new Wind());
		elements.put(Elements.WATER, new Water());
		elements.put(Elements.FIRE, new Fire());
		elements.put(Elements.EARTH,new Earth());

		init();
	}

	public void thingCollected(){
		thingsCollected++;
		SaveDataHandler.getInstance().setScore(thingsCollected);
	}


    public boolean[] getAvailableElements() {
        return availableElements;
    }
	public void setAvailableElements(boolean wind, boolean water, boolean fire, boolean earth) {
        boolean[] t ={wind,water,fire,earth};
        availableElements = t;
    }

    public void setPosition(Vec2 p){
        body.setTransform(p,body.getAngle());
    }

    public boolean[] getAvailableDirections() {
        return availableDirections;
    }

    public void setAvailableDirections(boolean[] availableDirections) {
        this.availableDirections = availableDirections.clone();
    }

    public void setAvailableElements(boolean[] b) {
        availableElements=b.clone();
    }

	private void init(){            //Sets the player to its starting-values
		state = Elements.EARTH;
		body.setFixedRotation(false);
        setDepth(0);
        setTexture(null);
		currentElement=elements.get(Elements.EARTH);

		FixtureDef sensorFD = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.5f, 0.2f, new Vec2(0, -1.3f), 0);

		sensorFD.isSensor = true;
		sensorFD.shape = shape;             //Attaches a "foot-sensor" to the players body that defects
		sensorFD.userData = "foot";         //collision with the ground
		jump = 0;

		FixtureDef frictionFD = new FixtureDef();
		shape.setAsBox(0.95f, 0.2f, new Vec2(0, -1.2f), 0);
		frictionFD.friction = 0.5f;
		frictionFD.shape = shape;           //Attaches a fixture to the bottom of the player that adds friction
		frictionFD.isSensor = true;         //when standing on the ground. Only active when fire, wind or water.
		frictionFD.density = 0f;
		frictionFD.userData = "friction";

		body.createFixture(sensorFD);
		body.createFixture(frictionFD);

		body.createFixture(currentElement.getFixture());
		setTexture(currentElement.getTexture());
		setRepeatedTextures(false);
		super.reload();
	}

	public void switchElement(Elements e) {
        if(state != e){
            if(currentElement!=null)resetContacts();
            Audio.Track t = elementTracks.get(state);
            if(t!=null)
                Audio.getSharedInstance().stop(t);          //Stops the track that was played before the switch
            state = e;
            t = elementTracks.get(state);
            if(t!=null)
                Audio.getSharedInstance().play(t);          //Changes track to the track of the current elements
            if(e != Elements.EARTH){
                body.setTransform(body.getPosition(), 0);
                body.setAngularVelocity(0);
                body.setFixedRotation(true);                //Stops the players body from rotating unless its earth
                body.getFixtureList().getNext().setSensor(false);
            }else{
                body.setFixedRotation(false);
                body.getFixtureList().getNext().setSensor(true);
            }
            setTexture(null);
            currentElement=elements.get(e);
            if(body.getFixtureList() !=null)
                body.destroyFixture(body.m_fixtureList);        //Destroys the current fixture of the body to
            body.createFixture(currentElement.getFixture());    //replace it with the current elements fixture
            setTexture(currentElement.getTexture());
            setContacts();
            super.reload();
        }


	}
	public Element getCurrentElement() {
        return currentElement;
	}

    public Elements getState(){
        return state;
    }

    private void resetContacts(){
        currentElement.update(this);
        ContactEdge ce = getBody().getContactList();
        while(ce!=null){
            if(ce.other.getUserData() instanceof Interactable){
                ((Interactable)ce.other.getUserData()).reset(this);
            }
            ce = ce.next;
        }
    }
	@Override
	protected PolygonShape getShape() {
		PolygonShape shape = getCurrentElement().getShape();
		if (shape == null) {
			return super.getShape();
		} else {
			return shape;
		}
	}

    private void setContacts(){
        currentElement.update(this);
        ContactEdge ce = getBody().getContactList();
        while(ce!=null){
            if(ce.other.getUserData() instanceof Interactable){
                ((Interactable)ce.other.getUserData()).reset(this);
            }
            ce = ce.next;
        }
    }
	@Override
	public void update() {
        currentElement.update(this);
    }

	public static void setJump(int i){
		jump += i;
	}

	public boolean canIJump(){
        return jump > 0;
	}

    @Override
    public void mouse(Vec2 pos) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
	public void up() {
		getCurrentElement().moveUp(this);
	}

	@Override
	public void down() {
		getCurrentElement().moveDown(this);
	}

	@Override
	public void left() {
		getCurrentElement().moveLeft(this);
	}

	@Override
	public void right() {
		getCurrentElement().moveRight(this);
	}

	@Override
	public void enter() {}

	@Override
	public void pause() {
		l.actionPerformed(new ActionEvent("",5,Elementum.MODEL));
	}
	@Override
	public void hotkey(int id) {

        if(id>=1&&id<5){
            if(!availableElements[id-1])return;
            switchElement(Elements.values()[id-1]);
        }

        if(id==19){
            l.actionPerformed(new ActionEvent(l,0, Elementum.MODEL));
        }
        if(id==9){
            body.applyForce(new Vec2(1000,0),body.getPosition());
        }
	}

    @Override
    public HashMap<KeyConfig.Action, Boolean> getRepeat() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
	public void createFixtureDef(FixtureDef def) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

}
