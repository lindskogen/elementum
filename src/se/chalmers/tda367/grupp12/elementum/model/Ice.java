package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-04-24
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */

public class Ice extends Entity implements Interactable, Destructible {
    private boolean active = false;
    private boolean isDestroyed = false;

	public Ice(World w, Vec2 pos) {
		super(w, BodyType.STATIC, pos);
        body.setUserData(this);
		FixtureDef fixture = new FixtureDef();
        body.setUserData(this);

		try {
			setTexture(FileManager.getInstance().getTexture("ice512.png"));
		} catch (SlickException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

		fixture.friction = 0.01f;
		fixture.shape = makeRectangle(1,1);
		body.createFixture(fixture);
		reload();
	}

    @Override
	public void interact(Player p){
		if(p.getState() == Player.Elements.FIRE){
			/*PolygonShape shape = (PolygonShape)body.getFixtureList().getShape();
			int count = shape.getVertexCount();
			Vec2[] vertices = shape.getVertices();
			for(int i = 0; i<count; i++){
                float sub = 0.1f/FPSManager.getSharedInstance().getFPS();
				vertices[i] = vertices[i].sub(new Vec2(sub,sub));
                if(vertices[i].length()<0.01f){
                    isDestroyed=true;
                    return;
                }
			}
			PolygonShape shrunk = new PolygonShape();
			shrunk.set(vertices, vertices.length);
            setShape(shrunk);
			reload();  */
            isDestroyed=true;
		}
	}

    @Override
    public void reset(Player p) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
	public void update(){

	}

    @Override
    public void setActive(boolean b) {
        this.active=b;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public boolean destroyed() {
        return isDestroyed;
    }
}
