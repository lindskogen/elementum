package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.World;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.controller.KeyConfig;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.FloatBuffer;
import java.util.*;
import java.util.List;
import java.util.Timer;

import static org.lwjgl.opengl.GL11.GL_LIGHT_MODEL_AMBIENT;
import static org.lwjgl.opengl.GL11.glLightModel;
import static se.chalmers.tda367.grupp12.elementum.model.Helpers.angle;
import static se.chalmers.tda367.grupp12.elementum.model.Helpers.rotate;

/**
 *   This comes with no guaranties what so ever. Your computer might implode, explode or get thrown in to orbit around mars
 *
 *
 *   This is how it usually works:
 *
 *   Hover over entity → Reveal helpers and make green.
 *   Hover over helper → (hover over entity) + increase the helpers size
 *   LClicked on entity  (hover over entity) + move entity
 *   LClicked on helper (Hover over helper) + move helper + entity-vector
 *   RCicked on entity → add vector inbetween closest two vectors
 *   Rclicked on helper → remove the helper and entity vector
 *   Scrolled: zoom
 *
 *   press z to save
 *   press hotkeys 1-9 to add entities.
 *   Hover over entity and press delete to remove entity
 *
 *   to add other shapes/entities hardcode it att the end of this file. GG tnx bye
 *
 *   PS: nullpointers is the usuall. And yes, apocalypse inc dataloss do happen, it's life. Good luck young padavan,
 *   \\// (live long, and prosper)
 *
 * Date: 4/17/13
 * Time: 1:45 PM
 */
public class LevelEditor implements Model, InputAction {
    private boolean active = false;
    private final ActionListener l;

    private final EditorCamera camera = new EditorCamera();
    private Level currentLevel = null;

    /* All the entities*/
    private List<Entity> transposedEntities = new ArrayList<Entity>();
    private HashMap<Entity,List<Entity>> entities = new HashMap<Entity, List<Entity>>();
    private static final int MAX_HELPERS = 100; //to avoid lagg

    /* Local variables so we know what to do */
    private Entity activeEntity = null;
    private Vec2 oldMouse = null;
    private boolean entityGrabbed = false;
    private boolean helperGrabbed = false;
    private boolean globalLight = false;
    private int hoverHelper = -1;
    private boolean canAddVector = true;

    private final Timer timer = new Timer("Vector change allowed");


    /*helper shapes*/
    private final float s1 = 0.05f+0.02f *camera.getCameraPosition().z;
    private final PolygonShape NOT_HOVER = Entity.makeRectangle(s1,s1);
    private final float s2 = 0.075f + 0.03f *camera.getCameraPosition().z;
    private final PolygonShape HOVER = Entity.makeRectangle(s2,s2);
    private Vec2 mousePos = new Vec2(0,0);

    private Entity start;
    private Entity stop;

    public LevelEditor(ActionListener l){
        this.l=l;
    }

    /*Loads in  the entities and their helpers*/
    private void loadCurrentLevel(){
        entities = new HashMap<Entity, List<Entity>>();
        for(Entity e:currentLevel.getEntities()){
            entities.put(e,getHelpers(e));
        }
        currentLevel.setWorldActive(active);
        start = new Surface(new World(new Vec2(0,0),false),currentLevel.getLevelProperties().getStartPos());
        start.setColor(Color.green);
        stop = new Surface(new World(new Vec2(0,0),false),currentLevel.getLevelProperties().getEndPos());
        stop.setColor(Color.red);
        start.setShape(Entity.makeRectangle(1,1));
        stop.setShape(Entity.makeRectangle(1,1));
        List<Entity> e = new ArrayList<Entity>();
        entities.put(start,e);
        entities.put(stop,e);

        transposedEntities.clear();
        /* Loading transposed enteties from pre and next level */
        List<String> levels = FileManager.getInstance().getLevels();
        Collections.sort(levels);
        int indexofcurrentlevel = levels.indexOf(currentLevel.getName());
        if(indexofcurrentlevel>0){
            Level pre = LevelFactory.createLevel(levels.get(levels.indexOf(currentLevel.getName())-1));
            Vec2 transp = currentLevel.getLevelProperties().getStartPos().sub(pre.getLevelProperties().getEndPos());
            pre.tranpsLevel(transp);
            pre.alphaLevel(0.5f);
            transposedEntities.addAll(pre.getEntities());
        }
        if(indexofcurrentlevel<levels.size()-1){
            Level next = LevelFactory.createLevel(levels.get(levels.indexOf(currentLevel.getName())+1));
            Vec2 transp = currentLevel.getLevelProperties().getEndPos().sub(next.getLevelProperties().getStartPos());
            next.tranpsLevel(transp);
            next.alphaLevel(0.5f);
            transposedEntities.addAll(next.getEntities());
        }
    }

    /*generates helpers (red dots) from an EntityHolder*/
    private List<Entity> getHelpers(Entity e){
        List<Entity> activeHelpers=new ArrayList<Entity>();
        int i = 0;
        for(Vec2 v:e.getVectors()){
            activeHelpers.add(new LevelEditorHelper(v.add(e.getBody().getPosition()),NOT_HOVER ));
            i++;
            if(i>MAX_HELPERS)break;
        }
        return activeHelpers;
    }
    /*Updates one helper*/
    private void updateHelper(Entity e, int helper){
        entities.get(e).get(helper).getBody().setTransform(
		        e.getRotatedVectors().get(helper).add(e.getBody().getPosition()), 0);
    }

    private float getMouseAngle(){
        return angle(new Vec2(0,1),mousePos);
    }



    @Override
    public List<Drawable> getDrawables() {
        List<Drawable> entitieList = new ArrayList<Drawable>();
        entitieList.addAll(entities.keySet());

        if(activeEntity!=null&&!entityGrabbed){
            entitieList.addAll(entities.get(activeEntity));
        }
        entitieList.addAll(transposedEntities);
        return entitieList;
    }

    @Override
    public void update() {
        if(Mouse.hasWheel()){
            float s = Mouse.getDWheel();
            if(s!=0){
                camera.zoom(s/100);
            }
        }
        currentLevel.update();
        /*get mouse position*/
        if(oldMouse==null)oldMouse=mousePos;

        if(!Mouse.isButtonDown(0)){
            if(helperGrabbed){
                entities.get(activeEntity).get(hoverHelper).setShape(HOVER);
                helperGrabbed=false;
            }
            entityGrabbed=false;
        }

        if(!helperGrabbed&&!entityGrabbed){
            /*set entity*/
            Entity oldActive = activeEntity;
            boolean oldHelperHovered =  hoverHelper!=-1&&
                    hoverHelper<entities.get(activeEntity).size()&&
                    entities.get(activeEntity).get(hoverHelper).getTransformedAndRotatedPolygon().contains(mousePos.x,mousePos.y);
            boolean oldEntityHovered = activeEntity!=null&&activeEntity.getTransformedAndRotatedPolygon().contains(mousePos.x,mousePos.y);
            if(!oldEntityHovered){
                int oldHover = hoverHelper;
                hoverHelper=-1;
                activeEntity=null;
                for(Entity e: entities.keySet()){
                    if(e.getTransformedAndRotatedPolygon().contains(mousePos.x,mousePos.y)){
                        activeEntity=e;
                        int index = 0;
                        for(Entity entityHelper:entities.get(activeEntity)){
                            if(entityHelper.getTransformedAndRotatedPolygon().contains(mousePos.x,mousePos.y)){
                                hoverHelper=index;
                                entityHelper.setShape(HOVER);
                                break;
                            }
                            index++;
                        }
                    }
                }
                if(oldHover!=-1&&oldActive!=null&&hoverHelper!=oldHover&&
                        hoverHelper<entities.get(oldActive).size()){
                    entities.get(oldActive).get(oldHover).setShape(NOT_HOVER);
                }
                if(activeEntity==null||oldActive!=activeEntity){
                    if(oldActive!=null&&oldActive.getBody().getType()!= BodyType.STATIC){
                        oldActive.getBody().setActive(true);
                    }
                    if(oldActive!=null)
                        oldActive.setCreative(false);
                }
            } else if (!oldHelperHovered){
                if(hoverHelper!=-1)entities.get(oldActive).get(hoverHelper).setShape(NOT_HOVER);
                hoverHelper = -1;
                helperGrabbed= false;
                int index = 0;
                for(Entity entityHelper:entities.get(activeEntity)){
                    if(entityHelper.getTransformedAndRotatedPolygon().contains(mousePos.x,mousePos.y)){
                        hoverHelper=index;
                        entityHelper.setShape(HOVER);
                        break;
                    }
                    index++;
                }
            }
        }

        /*Left clicked?*/
        if(Mouse.isButtonDown(0)){
            if(hoverHelper!=-1){ //helper clicked, modify the vector
                helperGrabbed=true;
                float rot = -activeEntity.getBody().getAngle();
                Vec2 v= rotate(mousePos.sub(oldMouse),rot);
                v = v.add(activeEntity.getVectors().get(hoverHelper));
                activeEntity.modifyVector(hoverHelper,v);
                entities.get(activeEntity).get(hoverHelper).setShape(NOT_HOVER);
            } else if(activeEntity!=null){ // Entity clicked, move the entity
                entityGrabbed=true;
                activeEntity.getBody().setTransform(activeEntity.getBody().getPosition().add(mousePos.sub(oldMouse)), activeEntity.getBody().getAngle());
                    activeEntity.reload();
            }
        }
        // right-clicked
        if(Mouse.isButtonDown(1)){
            if(hoverHelper!=-1&&canAddVector){ //Remove helper
                activeEntity.removeVector(hoverHelper);
                entities.get(activeEntity).remove(hoverHelper);
                hoverHelper=-1;
            } else if(activeEntity!=null&&canAddVector){ //Add vector to marked entity
                Vec2 newVectorPos =rotate(new Vec2(mousePos.x,mousePos.y).sub(activeEntity.getBody().getPosition()),-activeEntity.getBody().getAngle());
                activeEntity.addVector(newVectorPos,false,false);
                entities.put(activeEntity,getHelpers(activeEntity));
            }
            canAddVector = false;
            timer.schedule(new TimerTask(){
                @Override
                public void run() {
                    canAddVector = true;
                }
            }
            ,300);
        }

        /* The currently modified entity WILL not be in simulation*/
        if(activeEntity!=null&&activeEntity.getBody().getType()!= BodyType.STATIC){
            activeEntity.getBody().setActive(false);
            activeEntity.getBody().setLinearVelocity(new Vec2(0, 0));
            activeEntity.getBody().setAngularVelocity(0);
        }
        /* Update relevant helpers*/
        if(activeEntity!=null){
            for(int i = 0;i<entities.get(activeEntity).size();i++){
                updateHelper(activeEntity,i);
            }
            activeEntity.setCreative(true);
        }
        oldMouse = mousePos;
    }

    @Override
    public InputAction getInputAction() {
        return this;
    }

    @Override
    public CameraInterface getCamera() {
        return camera;
    }


    @Override
    public void mouse(Vec2 pos) {
        mousePos = pos;
    }

    /* Keyboard Actions */
    @Override
    public void up() {
        camera.move(0,1);
    }

    @Override
    public void down() {
        camera.move(0,-1);
    }

    @Override
    public void left() {
        camera.move(-1,0);
    }

    @Override
    public void right() {
        camera.move(1, 0);
    }

    @Override
    public void enter() {
        active = !active;
        currentLevel.setWorldActive(active);
    }

    @Override
    public void pause() {
        l.actionPerformed(new ActionEvent("",1, Elementum.MODEL));
    }

    @Override
    public void hotkey(int id) {
        switch(id){
            case 11:
                camera.tilt(0,1);
                break;
            case 12:
                camera.tilt(0,-1);
                break;
            case 13:
                camera.tilt(-1,0);
                break;
            case 14:
                camera.tilt(1,0);
                break;
            case 16:
                camera.resetTilt();
                break;
            case 20:
                LevelProperties old = currentLevel.getLevelProperties();
                currentLevel.setLevelProperties(new LevelProperties(old.getGravity(),old.getImgpath(),
                        start.getBody().getPosition(),stop.getBody().getPosition()));
                LevelFactory.saveLevel(currentLevel);
                currentLevel=LevelFactory.createLevel(currentLevel.getName());
                loadCurrentLevel();
                activeEntity=null;
                hoverHelper=-1;
                entityGrabbed=false;
                helperGrabbed=false;
	            break;
            case 17:
                if(activeEntity!=null && !(activeEntity instanceof Surface)){
                    currentLevel.removeEntity(activeEntity);
                    entities.remove(activeEntity);
                    activeEntity=null;
                    hoverHelper=-1;
                    entityGrabbed=false;
                    helperGrabbed=false;
                }
                break;
            case 19:
                break;
            case 18:
                FloatBuffer lmodel_ambient;
                globalLight=!globalLight;
                if(globalLight){
                    lmodel_ambient = BufferUtils.createFloatBuffer(4).put(new float[] { 0.2f,0.2f,0.2f, 1.0f });
                } else {
                    lmodel_ambient = BufferUtils.createFloatBuffer(4).put(new float[] { 1,1,1, 1.0f });
                }
                lmodel_ambient.rewind();
                glLightModel(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
                break;
            case 10:
                if(activeEntity!=null){
                    activeEntity.getBody().setTransform(activeEntity.getBody().getPosition(),
                            activeEntity.getBody().getAngle()-0.1f);
                }
                break;

            case 1:
                addBasicEntity(new Ground(currentLevel,mousePos));
                break;
            case 2:
                addBasicEntity(new Rock(currentLevel,mousePos));
                break;
            case 3:
                addBasicEntity(new Grass(currentLevel,mousePos));
                break;
            case 4:
                addBasicEntity(new LevelWater(currentLevel,mousePos));
                break;
	        case 5:
		        addBasicEntity(new Ice(currentLevel,mousePos));
		        break;
	        case 6:
		        addBasicEntity(new Crack(currentLevel,mousePos));
		        break;
            case 7:
                addBasicEntity(new Vine(currentLevel,mousePos));
                break;
	        case 8:
				break;
            case 0:
	            Vec2 mPos = mousePos;
	            List<String> entities = new ArrayList<String>();
	            entities.add("Ground");
	            entities.add("Rock");
	            entities.add("Grass");
	            entities.add("Water");
	            entities.add("Ice");
	            entities.add("Crack");
	            entities.add("Vine");
	            entities.add("Ele_water");
	            entities.add("Ele_fire");
	            entities.add("Ele_wind");
	            entities.add("Col_earth");
	            entities.add("Col_water");
	            entities.add("Col_fire");
	            entities.add("Col_wind");

	            JComboBox itemSelect = new JComboBox(Arrays.copyOf(entities.toArray(),entities.size(), String[].class));
	            itemSelect.setSelectedIndex(0);
	            JOptionPane.showMessageDialog(null, itemSelect, "Select an entity", JOptionPane.OK_OPTION | JOptionPane.PLAIN_MESSAGE);
	            int entity = itemSelect.getSelectedIndex();
	            switch (entity){
		            case 0:
						addBasicEntity(new Ground(currentLevel,mPos));
			            break;
		            case 1:
			            addBasicEntity(new Rock(currentLevel,mPos));
			            break;
		            case 2:
			            addBasicEntity(new Grass(currentLevel,mPos));
			            break;
		            case 3:
			            addBasicEntity(new LevelWater(currentLevel,mPos));
			            break;
		            case 4:
			            addBasicEntity(new Ice(currentLevel,mPos));
			            break;
		            case 5:
			            addBasicEntity(new Crack(currentLevel,mPos));
			            break;
		            case 6:
			            addBasicEntity(new Vine(currentLevel,mPos));
			            break;
		            case 7:
			            addBasicEntity(new UnlockWater(currentLevel,mPos));
			            break;
		            case 8:
			            addBasicEntity(new UnlockFire(currentLevel,mPos));
			            break;
		            case 9:
			            addBasicEntity(new UnlockWind(currentLevel,mPos));
			            break;
		            case 10:
			            addBasicEntity(new CollectableEarth(currentLevel,mPos));
			            break;
		            case 11:
			            addBasicEntity(new CollectableWater(currentLevel,mPos));
			            break;
		            case 12:
			            addBasicEntity(new CollectableFire(currentLevel,mPos));
			            break;
		            case 13:
			            addBasicEntity(new CollectableWind(currentLevel,mPos));
			            break;
	            }

                break;

        }
    }

    @Override
    public Map<KeyConfig.Action, Boolean> getRepeat() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Audio.Track getSong() {
        return null;
    }

    @Override
    public void reload(String s) {
        if(!s.equals("noreload")){
            l.actionPerformed(new ActionEvent("",0,Elementum.MODEL));
            currentLevel = LevelFactory.createLevel(s);
            loadCurrentLevel();
            l.actionPerformed(new ActionEvent("noreload", 6, Elementum.MODEL));
        }
    }


    private void addBasicEntity(Entity e){
        if(e instanceof Rock){
            e.setShape(Entity.makeRectangle(1f, 1f));
        } else if(!(e instanceof Collectable)){
            e.setShape(Entity.makeRectangle(2, 2));
        }
        entities.put(e,getHelpers(e));
        currentLevel.addEntity(e);    }
}