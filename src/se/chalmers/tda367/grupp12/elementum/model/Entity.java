package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.jbox2d.dynamics.*;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.lang.annotation.AnnotationFormatError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Date: 2013-04-10
 * Time: 17:59
 */
public abstract class Entity extends Drawable {
    public String texture;
    Image textureImage = null;
    protected final Body body;
    private Color color;
    private boolean repeatedTextures = true;
    private float depth = Drawable.STANDART_DEPTH;
    private boolean creativeMode = false;
    private float zvalue = PRIORITY_4;
    private Polygon2D polygon;
    private List<Vec2> vectors;
    private Vec2 gRotation = new Vec2(0,0);

    protected Entity(World w, BodyType type, Vec2 pos) {
        BodyDef def = new BodyDef();
        def.type = type;
        def.position = pos;
        body = w.createBody(def);
    }

    public static PolygonShape makeCircle(float radius, int roundness) {
        Vec2[] vectors = new Vec2[roundness];               //Creates a circle with the specified radius and roundness
        float degree = 360f / roundness;
        for (float i = 0, index = 0; index < roundness; i += degree, index++) {
            Vec2 vec = new Vec2((float) (radius * Math.cos(Math.toRadians(i))), (float) (radius * Math.sin(Math.toRadians(i))));
            vectors[(int) index] = vec;
        }
        PolygonShape circle = new PolygonShape();
        circle.set(vectors, roundness);
        return circle;
    }

    public static PolygonShape makeRectangle(float width, float height) {
        float centroidX = width / 2;
        float centroidY = height / 2;

        Vec2[] vectors = new Vec2[5];

        vectors[0] = new Vec2(-centroidX, -centroidY);
        vectors[1] = new Vec2(centroidX, -centroidY);
        vectors[2] = new Vec2(centroidX, centroidY);
        vectors[3] = new Vec2(-centroidX, centroidY);

        PolygonShape shape = new PolygonShape();
        shape.set(vectors, 4);
        return shape;
    }

    public static PolygonShape makeTriangle(float bottom, float height) {
        Vec2[] vectors = new Vec2[3];
        vectors[0] = new Vec2(-(bottom / 2), -(height / 2));
        vectors[1] = new Vec2(bottom / 2, -(height / 2));
        vectors[2] = new Vec2(bottom / 2, height / 2);
        PolygonShape triangle = new PolygonShape();
        triangle.set(vectors, 3);

        return triangle;
    }

    @Override
    public boolean isCreative() {
        return creativeMode;
    }

    public void setCreative(boolean c) {
        creativeMode = c;
    }

    @Override
    public boolean isOkayShape() {
        return getPolygon(true, true).contains(body.getPosition().x, body.getPosition().y) && Helpers.isConvexPolygon(getVectors());
    }

    public Body getBody() {
        return body;
    }

    protected void setz(float z) {
        this.zvalue = z;
    }

    public void setShape(org.jbox2d.collision.shapes.Shape shape) {
        FixtureDef newfd = new FixtureDef();
        Fixture oldf = body.getFixtureList();
        newfd.density = oldf.getDensity();
        newfd.friction = oldf.getFriction();
        newfd.shape = shape;
        newfd.isSensor = oldf.isSensor();
        newfd.restitution = oldf.getRestitution();
        body.destroyFixture(oldf);
        body.createFixture(newfd);
        reload();
    }

    public void reload() {
        polygon = loadPolygon();
        vectors = new ArrayList<Vec2>(loadVectors());
    }

    public List<Vec2> getVectors() {
        if (vectors == null) reload();
        return vectors;
    }

    public List<Vec2> getRotatedVectors() {
        Polygon2D p = getRotatedPolygon();
        List<Vec2> returnv = new ArrayList<Vec2>();
        for (int i = 0; i < p.npoints; i++) {
            returnv.add(new Vec2(p.xpoints[i], p.ypoints[i]));
        }
        return returnv;
    }

    public List<Vec2> getRotatedAndTranslatedVectors() {
        Polygon2D p = getTransformedAndRotatedPolygon();
        List<Vec2> returnv = new ArrayList<Vec2>();
        for (int i = 0; i < p.npoints; i++) {
            returnv.add(new Vec2(p.xpoints[i], p.ypoints[i]));
        }
        return returnv;
    }
	protected PolygonShape getShape() {
		return (PolygonShape) (this.body.getFixtureList().getShape());
	}

    private List<Vec2> loadVectors() {
        /* Loading vertixes from body */
        PolygonShape shape = getShape();
        ArrayList<Vec2> vertices = new ArrayList<Vec2>();
        Vec2[] vertic = shape.getVertices();
        vertices.addAll(Arrays.asList(vertic).subList(0, shape.getVertexCount()));
        return vertices;
    }

    public Polygon2D getPolygon() {
        if (polygon == null) {
            reload();
        }
        return polygon;
    }

    Polygon2D getPolygon(boolean translated, boolean rotated) {
        Polygon2D p = getPolygon();
        if (!translated && !rotated) {
            return getPolygon();
        }
        AffineTransform trans = null;
        if (translated) {
            Vec2 pos = body.getPosition();
            trans = AffineTransform.getTranslateInstance(pos.x, pos.y);
        }
        if (rotated) {
            AffineTransform rotate = AffineTransform.getRotateInstance(body.getAngle());
            if (trans == null) {
                trans = rotate;
            } else {
                trans.concatenate(rotate);
            }
        }
        PathIterator pi = p.getPathIterator(trans);
        Polygon2D returnp = new Polygon2D();
        for (int i = 0; i < p.npoints; i++) {
            float[] point = new float[2];
            pi.currentSegment(point);
            returnp.addPoint(point[0], point[1]);
            pi.next();
        }
        return returnp;
    }

    Polygon2D getRotatedPolygon() {
        return getPolygon(false, true);
    }

    public Polygon2D getTransformedAndRotatedPolygon() {
        return getPolygon(true, true);
    }

    public Polygon2D getTranslatedPolygon() {
        return getPolygon(true, false);
    }

    private Polygon2D loadPolygon() {
        Polygon2D polygon = new Polygon2D();
        // Get the nodes of the rock and add them to the polygon
        PolygonShape shape = getShape();
        Vec2[] vecList = shape.getVertices();
        for (int i = 0; i < shape.getVertexCount(); i++) {
            polygon.addPoint(((vecList[i].x)),
                    ((vecList[i].y)));
        }
        return polygon;
    }

    public abstract void update();

    public void createFixtureDef(FixtureDef def) {
        body.createFixture(def);
    }

    public void removeVector(int index) {
        if (vectors.size() > 3) { //We must have atleast 3 vectors
            Vec2[] newShapeVectors = new Vec2[vectors.size() - 1];
            for (int i = 0, r = 0; r < vectors.size(); i++, r++) {
                if (r != index)
                    newShapeVectors[i] = vectors.get(r);
                else
                    i--;
            }
            PolygonShape shape = new PolygonShape();
            shape.set(newShapeVectors, newShapeVectors.length);
            setShape(shape);
        }
    }

    public void addVector(Vec2 v, boolean deleteFirst, boolean addFirst) {
        List<Vec2> entityVectors = getRotatedVectors();
        int newVectorIndex = entityVectors.size() - 1;
        if (!deleteFirst || !addFirst) {
            entityVectors.add(entityVectors.get(0)); //add last, for checkin the closest vectors
                        /*calc where to place new vector*/
            float distance = Float.MAX_VALUE;
            for (int i = 1; i < entityVectors.size(); i++) {
                float tempd = Helpers.distance(entityVectors.get(i - 1), v) + Helpers.distance(entityVectors.get(i), v);
                if (tempd < distance) {
                    distance = tempd;
                    newVectorIndex = i;
                }
            }
            entityVectors = getVectors();
            entityVectors.add(newVectorIndex, v);
        }
        if (deleteFirst)
            entityVectors.remove(0);
        if (addFirst)
            entityVectors.add(v);
        Vec2[] newShapeVectors = new Vec2[entityVectors.size()];
        for (int i = 0; i < entityVectors.size(); i++) {
            newShapeVectors[i] = entityVectors.get(i);
        }
        PolygonShape shape = new PolygonShape();
        shape.set(newShapeVectors, newShapeVectors.length);
        setShape(shape);
    }

    public void modifyVector(int index, Vec2 newPos) {
        Vec2[] newShapeVectors = new Vec2[vectors.size()];
        for (int i = 0; i < newShapeVectors.length; i++) {
            if (i == index) {
                newShapeVectors[i] = newPos;
            } else {
                newShapeVectors[i] = vectors.get(i);
            }
        }
        PolygonShape shape = new PolygonShape();
        shape.set(newShapeVectors, vectors.size());
        setShape(shape);
    }
	public String toString() {
		Vec2 pos = body.getPosition();
		//Polygon2D pol = getRotatedPolygon();
		//System.out.println(pol.hashCode() + " " + pol.hashCode());
		String id = null;
		try {
			id = getClass().getSimpleName();
		} catch (NullPointerException e) {
			throw new AnnotationFormatError("No id defined for: " + getClass().getName());
		}

		return id + "," + pos.x + "," + pos.y + "," + getRotation().z;
	}

    @Override
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Image getTexture() {
        return this.textureImage;
    }

    public void setTexture(Image textureImage) {
        this.textureImage = textureImage;
    }

    @Override
    public boolean isRepeatedTextures() {
        return repeatedTextures;
    }

    protected void setRepeatedTextures(boolean r) {
        repeatedTextures = r;
    }

    @Override
    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    @Override
    public Vec3 getPosition() {
        return new Vec3(body.getPosition().x, body.getPosition().y, zvalue);
    }

    @Override
    public Vec3 getRotation() {
        return new Vec3(gRotation.x,gRotation.y,body.getAngle());
    }
    public void  setGRotation(Vec2 r){
        gRotation=r;
    }
}
