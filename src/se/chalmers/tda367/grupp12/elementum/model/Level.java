package se.chalmers.tda367.grupp12.elementum.model;


import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.contacts.Contact;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Level extends World implements Serializable, ContactListener{

    private String name;
	private final static Vec2 DEFAULT_GRAVITY = new Vec2(0, -9.8f);
	private ContentImage background, foreground;
    private final float bounds = 10000;
	private final List<Entity> objects;
    private boolean worldActive = true;
	private LevelProperties lvlProps;


    public Level(String name, LevelProperties props) {
		super(props.getGravity(), true);
        this.name=name;
	    lvlProps = props;
		objects = new ArrayList<Entity>();
		/*try {
			background = new ContentImage("bg","background.png");
			//foreground = new ContentImage("fg",imgpath);
		} catch(ContentImage.TextureNotFoundException e) {
			System.err.println("Level textures not found: \"" + e.getMessage() + "\"");
			background = null;
			foreground = null;
		}*/
		this.setContactListener(this);
	}

	public LevelProperties getLevelProperties() {
		return lvlProps;
	}

    public void setWorldActive(boolean b){
        worldActive=b;
        for(Entity e:objects)
            e.getBody().setActive(b);
    }

	public boolean addEntity(Entity ent) {
		return objects.add(ent);
	}

    public void removeEntity(Entity e){
        objects.remove(e);
        destroyBody(e.getBody());
    }
	public List<Entity> getEntities() {
		return objects;
	}

    /*
	public void draw() {
		if (background != null) {
			background.draw();
		}
		for (Entity entity : objects) {
			entity.draw();
		}
		if (foreground != null) {
			foreground.draw();
		}
	}
	*/

	public void update() {
		for (Entity entity : objects) {
            //System.out.print(e.getVectors().size()+" ");
			entity.update();
        }
        if(worldActive){
            cleanUp();
            //System.out.println(1/60+" "+1f/FPSManager.getSharedInstance().getFPS());
            //step(1/60f,100,100);
            step((1f/FPSManager.getSharedInstance().getFPS()),5,5);
            //step(0f,1,1);
        }
	}

    private void  cleanUp(){ //TODO confirm functionallity
        for(Entity e: getEntities()){
            if((e instanceof Destructible) && ((Destructible)e).destroyed()){
                removeEntity(e);
                cleanUp();
                return;
            }
            if(e.getPosition().x<-bounds||e.getPosition().x>bounds||e.getPosition().y<-bounds||e.getPosition().y>bounds ){
                removeEntity(e);
                cleanUp();
                return;
            }
        }
    }

    @Override
    public void beginContact(Contact contact) {
        Object userdataa = contact.getFixtureA().getBody().getUserData();
        Object userdatab = contact.getFixtureB().getBody().getUserData();
        if(userdataa instanceof Interactable && userdatab instanceof Player){
            ((Interactable)userdataa).setActive(true);
        }else if(userdataa instanceof Player && userdatab instanceof Interactable){
	        ((Interactable)userdatab).setActive(true);
        }

	    userdataa = contact.getFixtureA().getUserData();
	    userdatab = contact.getFixtureB().getUserData();
	    if(userdataa == "foot" && !contact.getFixtureB().isSensor()
			    || userdatab == "foot" && !contact.getFixtureA().isSensor()){
		    Player.setJump(1);
	    }
    }

    @Override
    public void endContact(Contact contact) {
        Object userdataa = contact.getFixtureA().getBody().getUserData();
        Object userdatab = contact.getFixtureB().getBody().getUserData();
        if(userdataa instanceof Interactable && userdatab instanceof Player){
	        ((Interactable)userdataa).reset(((Player)userdatab));
	        ((Interactable)userdataa).setActive(false);
        }else if(userdataa instanceof Player && userdatab instanceof Interactable){
	        ((Interactable)userdatab).reset(((Player)userdataa));
	        ((Interactable)userdatab).setActive(false);
        }

	    userdataa = contact.getFixtureA().getUserData();
	    userdatab = contact.getFixtureB().getUserData();
	    if(userdataa == "foot" && !contact.getFixtureB().isSensor()
			    || userdatab == "foot" && !contact.getFixtureA().isSensor()){
		    Player.setJump(-1);
	    }


    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        //To change getBody() of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        //To change getBody() of implemented methods use File | Settings | File Templates.
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLevelProperties(LevelProperties l){
        lvlProps = l;
        setGravity(l.getGravity());
    }

    public void tranpsLevel(Vec2 v){
        for(Entity entity: getEntities()){
            entity.getBody().setTransform(entity.getBody().getPosition().add(v),
                    entity.getBody().getAngle());
        }

    }
    public void alphaLevel(float alpha){
        for(Entity entity: getEntities()){
            entity.setColor(new Color(1,1,1,alpha));
            entity.setDepth(alpha==1?1:0);
        }
    }
}
