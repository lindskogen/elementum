package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.SpriteSheet;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;

/**
 * Date: 5/22/13
 * Time: 6:29 PM
 */
public class Animation {
    private org.newdawn.slick.Animation a;
    private Vec2 size;
    private Vec3 pos;
    private Drawable d;
    Animation(SpriteSheet s, int duration, Vec3 pos,Vec2 size){
        a = new org.newdawn.slick.Animation(s,duration);
        this.size=size;
        this.pos=pos;
    }
    public void update(){
        a.update(FPSManager.getSharedInstance().getElapsedTimeMillis());
        d = new DrawableImage(a.getCurrentFrame(),size,pos,null,null);
    }
    public Drawable getDrawable(){
        return d;
    }
    public Vec2 getSize(){
        return size;
    }
    public void setSize(Vec2 d){
        this.size=d;
    }
    public void setPos(Vec3 p){
        this.pos=p;
    }
    public Vec3 getPos(){
        return pos;
    }
}
