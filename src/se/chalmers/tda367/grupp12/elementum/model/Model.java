package se.chalmers.tda367.grupp12.elementum.model;

import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;

import java.util.List;

/**
 * Interface for game models.
 * Date: 4/17/13
 * Time: 1:30 PM
 */
public interface Model {
    public List<se.chalmers.tda367.grupp12.elementum.model.Drawable> getDrawables();
    public void update();
    public InputAction getInputAction();
    public CameraInterface getCamera();
    public void reload(String s);
    public Audio.Track getSong();
}
