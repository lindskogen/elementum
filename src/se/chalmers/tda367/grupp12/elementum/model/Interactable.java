package se.chalmers.tda367.grupp12.elementum.model;

import se.chalmers.tda367.grupp12.elementum.model.Player;

/**
 * Date: 2013-04-10
 * Time: 20:50
 */
public interface Interactable {
	public void interact(Player p);
    public void reset(Player p);
    public void setActive(boolean b);
    public boolean isActive();
}
