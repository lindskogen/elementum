package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.Image;

import java.awt.*;
import java.util.List;
import java.util.Vector;

public class DrawableImage extends Drawable {

    private Color color;
    private Image image;
    private Vec3 pos;
    private Vec3 rot = new Vec3(0,0,0);
    private List<Vec2> vectors = new Vector<Vec2>();

    public DrawableImage(Image image, Vec2 size, Vec3 pos, Color color,Vec3 rot) {
        this.color=color;
        this.image = image;
        this.pos = pos;
        if(rot!=null)this.rot=rot;

        vectors.add(new Vec2(0,0));
        vectors.add(new Vec2(0, (float) size.y));
        vectors.add(new Vec2((float) size.x, (float) size.y));
        vectors.add(new Vec2((float) size.x, 0));
    }

    public DrawableImage(Image image, Vec3 pos,List<Vec2> vectors) {
        this.image = image;
        this.pos = pos;
        this.vectors = vectors;
    }

    public void rotate(float r){
        for(Vec2 v:vectors){
            Helpers.rotate(v,r);
        }
    }

    @Override
    public List<Vec2> getVectors() {
        return vectors;
    }

    @Override
    public Image getTexture() {
        return image;
    }

    @Override
    public boolean isRepeatedTextures() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public float getDepth() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isCreative() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Vec3 getRotation() {
        return rot;
    }

    @Override
    public Vec3 getPosition() {
        return pos;
    }

    @Override
    public Color getColor() {
        return color;
    }

}
