package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;

/**
 * Date: 2013-05-14
 * Time: 16:33
 * The level resources that is saved locally when a level is created in the level editor
 */
public class LevelProperties {
	private Vec2 gravity = new Vec2(0,-9.8f);
	private String imgpath;
	private Vec2 start = new Vec2(0,0), end =new Vec2(10,0);


	public LevelProperties() {}

	public LevelProperties(Vec2 gravity) {
		if (gravity != null) {
			this.gravity = gravity;
		}
	}

	public LevelProperties(Vec2 gravity, String imgpath, Vec2 playerStart, Vec2 playerEnd) {
		this(gravity);
		this.imgpath = imgpath;
		this.start = playerStart;
		this.end = playerEnd;
	}

	public Vec2 getGravity() {
		return gravity;
	}
	public String getImgpath() {
		return imgpath;
	}
	public Vec2 getStartPos() {
		return start;
	}
	public Vec2 getEndPos() {
		return end;
	}
}
