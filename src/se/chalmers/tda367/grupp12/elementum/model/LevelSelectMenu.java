package se.chalmers.tda367.grupp12.elementum.model;

import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Level select menu for selecting level to play and level to edit in leveleditor
 * Date: 5/22/13
 * Time: 6:24 PM
 */
public class LevelSelectMenu extends MenuModel implements ActionListener {
    private static List<MenuAction> getMenuItems(){
        List<MenuAction> menuOptions = new ArrayList<MenuAction>();
        for(String s: FileManager.getInstance().getLevels()){
            menuOptions.add(new MenuAction(s,new ActionEvent(s,6,Elementum.MODEL)));
        }
        menuOptions.add(new MenuAction("Main Menu",new ActionEvent("",1,Elementum.MODEL)));
        return menuOptions;
    }
    private static List<MenuAction> getValidatedMenuItems(){
        List<MenuAction> menuOptions = new ArrayList<MenuAction>();
        for(String s: FileManager.getInstance().getLevels()){
            menuOptions.add(new MenuAction("Level: "+s,new ActionEvent(s,2,Elementum.MODEL)));
        }
        menuOptions.add(new MenuAction("Main Menu",new ActionEvent("",1,Elementum.MODEL)));
        return menuOptions;
    }
    private CameraInterface camera = new DummyCamera();

    public LevelSelectMenu(ActionListener l,boolean isLevelEditor){
        super(l,isLevelEditor?getMenuItems():getValidatedMenuItems(),"title2.jpg",isLevelEditor?"Level Editor":"Select Level");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Drawable> getDrawables() {
        List<Drawable> d = new ArrayList<Drawable>();
        d.addAll(super.getDrawables());
        return d;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        super.update();
    }

}
