package se.chalmers.tda367.grupp12.elementum.model;

/**
 * Date: 5/12/13
 * Time: 2:40 AM
 */
public interface Destructible {
    public boolean destroyed();
}
