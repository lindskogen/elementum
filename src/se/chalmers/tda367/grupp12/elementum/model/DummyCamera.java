package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec3;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.util.glu.Project.gluLookAt;

/**
 * Date: 5/22/13
 * Time: 6:22 PM
 */
public class DummyCamera implements CameraInterface {
    private Vec3 cameraPosition = new Vec3(0, 0, 50.0f);
    private Vec3 cameraAim = new Vec3(0.0f, 0.0f, 0.0f);
    private Vec3 cameraOrientation = new Vec3(0.0f, 1.0f, 0.0f);

    public void setCameraPosition(Vec3 pos){
        cameraPosition=cameraAim=pos;
    }
    public void lookAt(){
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(cameraPosition.x,cameraPosition.y,cameraPosition.z,
                cameraAim.x,cameraAim.y,cameraAim.z,
                cameraOrientation.x,cameraOrientation.y,cameraOrientation.z);
    }

    @Override
    public Vec3 getCameraPosition() {
        return new Vec3(cameraPosition.x,cameraPosition.y,cameraPosition.z);
    }
}
