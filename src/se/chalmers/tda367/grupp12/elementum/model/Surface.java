package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Animation;

/**
 * Date: 5/12/13
 * Time: 9:27 PM
 */
public class Surface extends Entity implements Interactable{

    /*Surf settings*/
    private boolean sticky;
    private boolean triggerOnce;

    /*Audio*/
    private boolean playAtleastOnce;
    private boolean repeat;
    private String audio;

    /*Player effetcs*/
    private boolean maintainOldElementOptions;
    private boolean[] allowedElements;
    private boolean maintainOldDirectionOptions;
    private boolean[] allowedDirections;

    /*Animation*/
    private Animation animation = null;

    /*Vector effects*/
    private boolean maintainSpeedWhenLeaving;
    private Vec2 force;

    public Surface(World w, Vec2 pos) {
        super(w, BodyType.STATIC, pos);
        FixtureDef fd = new FixtureDef();
        fd.density = 0;
        fd.friction = 0;
        fd.restitution = 0;
        fd.shape = makeRectangle(10,10);
        fd.isSensor = true;
        body.createFixture(fd);
    }

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public void setTriggerOnce(boolean triggerOnce) {
        this.triggerOnce = triggerOnce;
    }

    public void setPlayAtleastOnce(boolean playAtleastOnce) {
        this.playAtleastOnce = playAtleastOnce;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public void setMaintainOldElementOptions(boolean maintainOldElementOptions) {
        this.maintainOldElementOptions = maintainOldElementOptions;
    }

    public void setAllowedElements(boolean[] allowedElements) {
        this.allowedElements = allowedElements;
    }

    public void setMaintainOldDirectionOptions(boolean maintainOldDirectionOptions) {
        this.maintainOldDirectionOptions = maintainOldDirectionOptions;
    }

    public void setAllowedDirections(boolean[] allowedDirections) {
        this.allowedDirections = allowedDirections;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public void setMaintainSpeedWhenLeaving(boolean maintainSpeedWhenLeaving) {
        this.maintainSpeedWhenLeaving = maintainSpeedWhenLeaving;
    }

    public void setForce(Vec2 force) {
        this.force = force;
    }

    @Override
    public void interact(Player p) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void reset(Player p) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setActive(boolean b) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isActive() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
