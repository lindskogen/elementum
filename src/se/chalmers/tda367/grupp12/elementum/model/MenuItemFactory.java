package se.chalmers.tda367.grupp12.elementum.model;

import java.util.ArrayList;

/**
 * Class for creating menus, not finished implementation, not in use
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 4/23/13
 * Time: 4:03 PM
 */
public class MenuItemFactory {

	private final ArrayList<MenuItem> allMenuItems = new ArrayList<MenuItem>();

	public MenuItemFactory(){
		String name="test";
		String fileName="menutest.png";
		String selectedFilename = "selectedmenutest.png";
		allMenuItems.add(createMenuItem(name,fileName, selectedFilename));
		allMenuItems.add(createMenuItem(name,fileName, selectedFilename));
		allMenuItems.add(createMenuItem(name,fileName, selectedFilename));
		allMenuItems.add(createMenuItem(name,fileName, selectedFilename));
	}

	private MenuItem createMenuItem(String name, String fileName, String selectedFileName){
		return new MenuItem("name", fileName, selectedFileName);
	}

	public ArrayList<MenuItem> getMenu(){
		return allMenuItems;
	}
}
