package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Date: 2013-04-10
 * Time: 20:51
 */

public class Crack extends Entity implements Interactable {
	private FixtureDef fd = new FixtureDef();
	private boolean active = false;

	public Crack(World w, Vec2 pos) {
		super(w,BodyType.STATIC, pos);
		body.setUserData(this);
		try {
			setTexture(FileManager.getInstance().getTexture("crack.png"));
		} catch (SlickException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
        setz(Drawable.PRIORITY_1);
		fd.shape= makeRectangle(1,1);
		fd.friction = 1f;
		body.createFixture(fd);
		reload();
	}

	public void interact(Player p){
		if(p.getState() == Player.Elements.WATER){
			body.getFixtureList().setSensor(true);
			boolean[] d = new boolean[]{true,true,true,true};
			boolean[] e = new boolean[]{false, true, false, false};
			p.setAvailableDirections(d);
			p.setAvailableElements(e);
			p.getBody().applyForce(new Vec2(0, 9.8f * p.getBody().getMass()), p.getBody().getPosition());
			p.getBody().setLinearVelocity(new Vec2(p.getBody().getLinearVelocity().x * 0.95f,p.getBody().getLinearVelocity().y*0.95f));
		}
	}

    @Override
    public void reset(Player p) {
	    if(body.getFixtureList() != null){
		    body.getFixtureList().setSensor(false);
		    boolean[] d = new boolean[]{true,false,false,true};
		    boolean[] e = new boolean[]{true, true, true, true};
		    p.setAvailableDirections(d);
		    p.setAvailableElements(e);
	    }
    }

    @Override
    public void setActive(boolean b) {
        this.active=b;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
	public void update() {

	}
}
