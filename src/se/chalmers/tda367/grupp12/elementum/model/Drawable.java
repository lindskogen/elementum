package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.Image;

import java.awt.*;
import java.util.List;

/**
 * Date: 5/6/13
 * Time: 11:26 AM
 */
public abstract class Drawable implements Comparable{

    public static final float PRIORITY_0 = 0f;
    public static final float PRIORITY_1 = 0.01f;
    public static final float PRIORITY_2 = 0.02f;
    public static final float PRIORITY_3 = 0.03f;
    public static final float PRIORITY_4 = 0.04f;
    public static final float PRIORITY_5 = 0.05f;
    public static final float PRIORITY_6 = 0.06f;
    public static final float PRIORITY_7 = 0.07f;
    public static final float PRIORITY_8 = 0.08f;
    public static final float STANDART_DEPTH = 1;

    public abstract List<Vec2> getVectors();
    public abstract Image getTexture();
    public abstract boolean isRepeatedTextures();
    public abstract float getDepth();
    public abstract boolean isCreative();
    public boolean isOkayShape(){
        return true;
    }
    public Vec3 getRotation(){
        return new Vec3(0,0,0);
    };
    public abstract Vec3 getPosition();
    public abstract Color getColor();
    boolean isTransp(){
        return false;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Drawable){
            Drawable comp = (Drawable)o;
            if(comp.isTransp() && isTransp() || comp.getDepth()!=0 && getDepth()!=0 ||
                    (!comp.isTransp() && !isTransp() && comp.getDepth()==0 && getDepth()==0 )){
                return comp.getPosition().z > getPosition().z? -1:comp.getPosition().z<getPosition().z?1:0;
            } else {
                return isTransp()?-1:((Drawable) o).isTransp()?1: getDepth()!=0?-1:1;
            }
        } else {
            throw new IllegalArgumentException("Not a Drawable");
        }
    }
}
