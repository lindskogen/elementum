package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;

/**
 * Date: 2013-04-10
 * Time: 23:01
 */

public class Rock extends Entity implements Destructible {

	private static final float DENSITY = 2.66f;
	private static final float FRICTION = 1f;
    private final Vec2 startpos;

	public Rock(World w, Vec2 pos){
		super(w, BodyType.DYNAMIC, pos);
		startpos = pos;
		try {
            setTexture(FileManager.getInstance().getTexture("ground_cartoon.jpg"));
		} catch (SlickException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
        setColor(Color.lightGray);
		FixtureDef fd = new FixtureDef();
		fd.density = DENSITY;
		fd.friction = FRICTION;
		fd.restitution = 0;
		fd.shape = makeRectangle(1,1);
		body.createFixture(fd);
	}

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean destroyed() {
        //return Math.abs(getBody().getLinearVelocity().x) + Math.abs(getBody().getLinearVelocity().y) > 10;
        boolean d = Helpers.distance(body.getPosition(),startpos)>3;
        if(d) Audio.getSharedInstance().play(Audio.SMASH);
        return  d;
    }
}
