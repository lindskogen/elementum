package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * An not complete class for drawing images, currently not in use.
 */
public class ContentImage extends Drawable {

	private final String name;
	private Image image;
    private List<Vec2> v;

    /**
     * Create an image
     * @param name  the name
     * @param imageName the path to the image
     * @throws TextureNotFoundException in case there is no such image
     * @throws SlickException in case of error loading in image
     */
	public ContentImage(String name, String imageName) throws TextureNotFoundException, SlickException {
        this(name,FileManager.getInstance().getImage(imageName));
	}

    /**
     * Create an image
     * @param name the name
     * @param image the image
     */
	public ContentImage(String name, Image image){
		this.name = name;
		this.image = image;

        float scale = 60f;
        float h = image.getHeight()/scale;
        float w = image.getWidth()/scale;
        v = new ArrayList<Vec2>();
        v.add(new Vec2(0,0));
        v.add(new Vec2(0,h));
        v.add(new Vec2(w,h));
        v.add(new Vec2(w,0));
	}

    /** {@inheritDoc}
     */
    @Override
    public List<Vec2> getVectors() {
        return v;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Image getTexture() {
        return image;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRepeatedTextures() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float getDepth() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCreative() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Vec3 getPosition() {
        return new Vec3(0,0,0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Color getColor() {
        return null;
    }

    /**
     * get image name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * An exception in case the texture specified does not exist
     */
    private class TextureNotFoundException extends Exception {
		public TextureNotFoundException(String path) {
			super(path);
		}
	}

    /**
     * Set a new image
     * @param img is the new image
     */
	public void setImage(Image img){
		this.image = img;
	}

    /**
     * Get the image
     * @return the image
     */
	public Image getImage(){
		return image;
	}
}
