package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import se.chalmers.tda367.grupp12.elementum.utils.FPSManager;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Date: 5/7/13
 * Time: 2:18 PM
 */

public class Grass extends Entity implements Interactable{
    private Animation animation;
    private boolean active = false;
    public Grass(World w,Vec2 pos){
        super(w, BodyType.STATIC, pos);
        body.setUserData(this);
        setDepth(0);
        setRepeatedTextures(false);
        setz(Entity.PRIORITY_3);
        createFixtureDef();

        try {
            org.newdawn.slick.Image sprite = FileManager.getInstance().getSprite("grass.png");
            SpriteSheet t = new SpriteSheet(sprite,256,256);
            animation = new Animation(t,250);
        }catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    @Override
    public void update() {
        setTexture(animation.getCurrentFrame());
    }

    @Override
    public void interact(Player p) {
        if(p.getState() == Player.Elements.WIND)
            animation.update(FPSManager.getSharedInstance().getElapsedTimeMillis());
    }

    @Override
    public void reset(Player p) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    void createFixtureDef(){
        FixtureDef fd = new FixtureDef();
        fd.shape=Entity.makeRectangle(1,1);
        fd.friction = 1f;
        fd.isSensor = true;
        body.createFixture(fd);
        reload();
    }

    @Override
    public void setActive(boolean b) {
        this.active=b;
    }

    @Override
    public boolean isActive() {
        return active;
    }
}
