package se.chalmers.tda367.grupp12.elementum.model;


import org.jbox2d.common.Vec2;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.controller.InputAction;
import se.chalmers.tda367.grupp12.elementum.utils.Audio;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;
import se.chalmers.tda367.grupp12.elementum.utils.SaveDataHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Universe implements Model, ActionListener {
	public static final String MENU ="Open menu";
    private final ActionListener l;
	private Level activeLevel;
    private Level preLevel;
    private Level nextLevel;
	private Player player;
    private PlayerCamera camera;
	//private MenuManager menu;
    private List<String> levels = new ArrayList<String>();
	private int indexOfCurrentLevel;

    public Universe(ActionListener l){
        levels = FileManager.getInstance().getLevels();
        this.l = l;
    }
	private void init(){
        if(activeLevel == null)
            if(SaveDataHandler.getInstance().getLevel().equals(""))
                init(FileManager.getInstance().getLevels().get(0));
            else
                init(SaveDataHandler.getInstance().getLevel());
    }

    private void init(final String s){
        l.actionPerformed(new ActionEvent("", 0, Elementum.MODEL));
        setLevel(s);
        l.actionPerformed(new ActionEvent("noreload", 2, Elementum.MODEL));
    }

	public void setLevel(String s){
		activeLevel = LevelFactory.createLevel(s);
		SaveDataHandler.getInstance().setLevel(s);
		initiatePlayer();

		// Change listener to player for the current level
		this.l.actionPerformed(new ActionEvent(player,0,Elementum.LISTENER));

		initiateCamera();

		updateLevelList();
		setPreviousLevel();
		setNextLevel();
	}

	private void initiatePlayer(){
		Vec2 startPosition = activeLevel.getLevelProperties().getStartPos();

		if(player == null){
			player = new Player(activeLevel, startPosition, this.l);
		}else{
			player = new Player(activeLevel, startPosition, this.l, player.getState());
		}
	}

	private void initiateCamera(){
		if(camera == null){
			camera = new PlayerCamera(player);
		}else{
			camera.setPlayer(player);
		}
	}

	private void setPreviousLevel(){
		if(indexOfCurrentLevel > 0){
			preLevel = LevelFactory.createLevel(levels.get(levels.indexOf(activeLevel.getName())-1));
			Vec2 transp = activeLevel.getLevelProperties().getStartPos().sub( preLevel.getLevelProperties().getEndPos() );
			preLevel.tranpsLevel(transp);
		}else{
			preLevel = null;
		}
	}

	private void setNextLevel(){
		if(indexOfCurrentLevel < levels.size()-1){
			nextLevel = LevelFactory.createLevel(levels.get(levels.indexOf(activeLevel.getName())+1));
			Vec2 transp = activeLevel.getLevelProperties().getEndPos().sub(nextLevel.getLevelProperties().getStartPos());
			nextLevel.tranpsLevel(transp);
		} else{
			nextLevel = null;
		}
	}

	private void updateLevelList(){
		levels = FileManager.getInstance().getLevels();
		Collections.sort(levels);
		indexOfCurrentLevel = levels.indexOf(activeLevel.getName());
	}

	public Level getActiveLevel() {
		return activeLevel;
	}

    @Override
    public List<Drawable> getDrawables() {
        List<Drawable> d = new ArrayList<Drawable>();
        d.add(player);
        d.addAll(activeLevel.getEntities());
	    //if(menu.isActive())d.addAll(menu.getDrawables());
        if(preLevel!=null)d.addAll(preLevel.getEntities());
        if(nextLevel!=null)d.addAll(nextLevel.getEntities());
        return d;
    }

    public void update(){
	    player.update();
        camera.updateCamera();
        activeLevel.update();
		for(Entity entity: activeLevel.getEntities()){
			if(entity instanceof Interactable && ((Interactable)entity).isActive()){
                ((Interactable)entity).interact(player);
            }
        }
        Vec2 end = activeLevel.getLevelProperties().getEndPos();

	    // Check if player is in current levels end position
        if(player.getTransformedAndRotatedPolygon().contains(end.x,end.y)){
            System.out.println("Level cleared!!!"); //TODO: Temporary.
            levelCleared();
        }
    }

    private void levelCleared() {
        List<String>levels = FileManager.getInstance().getLevels();
        if(levels.indexOf(activeLevel.getName())>levels.size()-2){
            l.actionPerformed(new ActionEvent("",7,Elementum.MODEL));
            SaveDataHandler.getInstance().reset();
        } else {
            setLevel(levels.get(levels.indexOf(activeLevel.getName()) + 1));
        }
    }

    @Override
    public InputAction getInputAction() {
        return player;
    }

    @Override
    public CameraInterface getCamera() {
        return camera;
    }

    @Override
    public void reload(String s) {
        if(!s.equals("noreload")){
            if(s.equals("new game")){
                SaveDataHandler.getInstance().reset();
                init(FileManager.getInstance().getLevels().get(0));
            }
            else if(!s.equals("")){
                init(s);
            }else{
                init();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public Audio.Track getSong() {
        return Audio.LEVEL;
    }
}
