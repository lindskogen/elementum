package se.chalmers.tda367.grupp12.elementum.model;

import java.awt.event.ActionEvent;

/**
 * Data structure class for a menu item. Contains the name of the menu item as well as the to be runned event
 * Date: 5/24/13
 * Time: 6:47 PM
 */
public class MenuAction {
    private ActionEvent e;
    private String s;
    public MenuAction(String s,ActionEvent e){
        this.s=s;
        this.e=e;
    }

    public ActionEvent getE() {
        return e;
    }

    public String getS() {
        return s;
    }
}
