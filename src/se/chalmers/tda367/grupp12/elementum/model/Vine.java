package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Date: 2013-04-10
 * Time: 20:55
 */

public class Vine extends Entity implements Interactable {

    private boolean active = false;

	//Creates a "standard" shaped vine
	public Vine(World w, Vec2 pos) {
		super(w, BodyType.STATIC, pos);
        body.setUserData(this);
		setz(PRIORITY_1);
        setDepth(0);
        FixtureDef fd = new FixtureDef();
        fd.userData = "Vine";
        fd.shape = makeRectangle(1,1);
        fd.isSensor = true;
        body.createFixture(fd);
        try {
            setTexture(FileManager.getInstance().getTexture("vine-texture-groundstyle.png"));
        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

	public void interact(Player p){
		if(p.getCurrentElement() instanceof Fire){
			p.setAvailableDirections(new boolean[]{true,true,true,true});
			p.getBody().applyForce(new Vec2(0, 9.8f * p.getBody().getMass()), p.getBody().getPosition());
			p.getBody().setLinearVelocity(new Vec2(p.getBody().getLinearVelocity().x * 0.95f,p.getBody().getLinearVelocity().y*0.95f));
		}
	}

    @Override
    public void reset(Player p) {
	    p.setAvailableDirections(new boolean[]{true,false,false,true});
    }

	@Override
	public void update() {
		// TODO: Implement method
	}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
