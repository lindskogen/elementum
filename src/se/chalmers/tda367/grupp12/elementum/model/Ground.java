package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-04-15
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */

public class Ground extends Entity {
	public Ground(World w, Vec2 pos){
		super(w, BodyType.STATIC, pos);
		try {
			setTexture(FileManager.getInstance().getTexture("ground_cartoon.jpg"));
		} catch (SlickException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		FixtureDef fd = new FixtureDef();
		fd.shape= makeRectangle(1,1);
		fd.friction = 1f;
		body.createFixture(fd);
		reload();
	}

	public void update() {

    }
}
