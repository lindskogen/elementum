package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import se.chalmers.tda367.grupp12.elementum.utils.FileManager;


/**
 * Date: 2013-04-10
 * Time: 20:59
 */
public class Earth extends Element {

	private static final float SPEED = 10;
	private static final float MAX_SPEED = 10;
    private Image texture;

	public Earth() {            //Default constructor
        try {
	        texture = FileManager.getInstance().getTexture("earth.png");
        } catch (SlickException e) {                    //Sets the texture of the element
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        fixdef = new FixtureDef();
        //PolygonShape shape = Entity.makeCircle(1.5f, 17);
		CircleShape shape = new CircleShape();
		shape.m_radius = 1.5f;
		fixdef.userData = "Earth";
        fixdef.shape = shape;
		fixdef.density = 10f;
		fixdef.friction = 1f;
	}
	@Override
	public PolygonShape getShape() {
		return Entity.makeCircle(1.5f, 17);
	}
    @Override
    public Image getTexture(){
        return texture;
    }
	@Override
	public void moveLeft(Player p) {
        p.getBody().applyAngularImpulse(SPEED);
	}

	@Override
	public void moveRight(Player p) {
        p.getBody().applyAngularImpulse(-SPEED);
	}
	@Override
	public void moveDown(Player p) {
		p.getBody().applyLinearImpulse(new Vec2(0 ,-40), p.getBody().getPosition());
	}

	@Override
	public void moveUp(Player p) {
		// TODO: Implement method
	}


	@Override
	public void update(Player p) {      //Stops the player from exceeding the max velocity
        if(p.getBody().getAngularVelocity()>MAX_SPEED) p.getBody().setAngularVelocity(MAX_SPEED);
        if(p.getBody().getAngularVelocity()<-MAX_SPEED) p.getBody().setAngularVelocity(-MAX_SPEED);
	}
}
