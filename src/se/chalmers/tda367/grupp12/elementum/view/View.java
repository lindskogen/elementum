package se.chalmers.tda367.grupp12.elementum.view;


import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.Image;
import se.chalmers.tda367.grupp12.elementum.model.Drawable;
import se.chalmers.tda367.grupp12.elementum.model.Entity;
import se.chalmers.tda367.grupp12.elementum.model.Model;
import se.chalmers.tda367.grupp12.elementum.model.CameraInterface;
import se.chalmers.tda367.grupp12.elementum.model.Helpers;

import java.awt.*;
import java.util.Collections;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.Project.gluPerspective;

/**
 * This class makes all the rendering continually drawing the model to the screen using OpenGL
 */
public final class View {
    private CameraInterface camera;     //Camera
    private Model model;                //Model to draw
    private boolean viewSet = false;

    /**
     * Sets the model to draw
     * @param m is the model
     */
    public void setModel(Model m){
        this.model=m;
        this.camera=m.getCamera();
        viewSet = true;
    }

    /**
     * Inits the display and sets all OpenGL properties
     * @param fullscreen if it shall be fullscreen
     * @throws LWJGLException if something goes wrong in OpenGL initzialisation
     */
    public View(boolean fullscreen) throws LWJGLException {

        /*Init display*/
        if(fullscreen) {
            Display.setDisplayMode(Display.getAvailableDisplayModes()[0]);
            Display.setFullscreen(true);
        } else {
            Display.setDisplayMode(new DisplayMode(1400, 800));
        }
        Display.create();
        /*Init gl*/
        reshape(Display.getWidth(), Display.getHeight());

        glMatrixMode(GL_PROJECTION);
        glClearColor(148f/255,224f/255,242f/255,0);   // Black Background
        glClearDepth(100.0f);                   // clear Depth Buffer Setup


        glEnable(GL_DEPTH_TEST);                // Enables Depth Testing

        glEnable(GL_BLEND); //Enable alpha blending
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);  //Enable alpha blending

    }

    /**
     * Method for drawing a drawable
     * @param e the object to draw
     */
    private static void  drawDrawable(Drawable e){
        List<Vec2> vectors = e.getVectors();
        int vcount = vectors.size();
        float xpos[]= new float[vcount+1];
        float ypos[]= new float[vcount+1];
        for(int i = 0;i<vcount;i++){
            xpos[i]=vectors.get(i).x;
            ypos[i]=vectors.get(i).y;
        }

        float xmin = Float.MIN_VALUE;
        float xmax = Float.MAX_VALUE;
        float ymin = Float.MIN_VALUE;
        float ymax = Float.MAX_VALUE;
        float txmin=0;
        float tymin=0;
        float txspan=0;
        float tyspan=0;

        Image texture = null;
        org.newdawn.slick.Color.white.bind();
        if(e.getTexture()!=null){
            texture = e.getTexture();
            texture.getTexture().bind();
            glEnable(GL_TEXTURE_2D);

        } else {
            glDisable(GL_TEXTURE_2D);
        }

    /* Setting max n min values */
        if(texture!=null&&!e.isRepeatedTextures()){

            txmin = texture.getTextureOffsetX();
            float txmax = txmin + texture.getTextureWidth();
            txspan = txmax - txmin;
            tymin = texture.getTextureOffsetY();
            float tymax = tymin + texture.getTextureHeight();
            tyspan = tymax - tymin;

            for(int i=0;i<vcount;i++){
                if(xmin==Float.MIN_VALUE){
                    xmin=xpos[i];
                } else if(xmin>xpos[i]){
                    xmin=xpos[i];
                }
                if(xmax==Float.MAX_VALUE){
                    xmax=xpos[i];
                } else if(xmax<xpos[i]){
                    xmax=xpos[i];
                }
                if(ymin==Float.MIN_VALUE){
                    ymin=ypos[i];
                } else if(ymin>ypos[i]){
                    ymin=ypos[i];
                }
                if(ymax==Float.MAX_VALUE){
                    ymax=ypos[i];
                } else if(ymax<ypos[i]){
                    ymax=ypos[i];
                }
            }
        }

        /*Draw*/
        Color c = e.getColor();
        if(c!=null){
            setColor(c);
        } else {
            setColor(Color.WHITE);
        }
        glPushMatrix();
        glTranslatef(e.getPosition().x,e.getPosition().y,e.getPosition().z);
        //System.out.println(e.getClass());
        glRotatef((float)Math.toDegrees(e.getRotation().x),1,0,0);
        glRotatef((float)Math.toDegrees(e.getRotation().y),0,1,0);
        glRotatef((float)Math.toDegrees(e.getRotation().z),0,0,1);
        glBegin(GL_POLYGON);
        for(int i=0;i<vcount;i++){
            if(texture!=null){
                if(!e.isRepeatedTextures()){
                    float x = (xpos[i]+Math.abs(xmin))/((Math.abs(xmin)+Math.abs(xmax)));
                    x=x*txspan+txmin;
                    float y = (ypos[i]+Math.abs(ymin))/((Math.abs(ymin)+Math.abs(ymax)));
                    y=y*tyspan+tymin;
                    glTexCoord2f(x, y);
                } else {
                    glTexCoord2f(xpos[i]/5 , ypos[i]/5); //hacksolution TODO fix
                }

            }
            glVertex2f(xpos[i], ypos[i]);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(e.getPosition().x,e.getPosition().y,e.getPosition().z);
        glRotatef((float)Math.toDegrees(e.getRotation().x),1,0,0);
        glRotatef((float)Math.toDegrees(e.getRotation().y),0,1,0);
        glRotatef((float)Math.toDegrees(e.getRotation().z),0,0,1);
        if(e.getDepth()!=0){
            for(int i=1;i<vcount;i++){
                quad(vectors.get(i),vectors.get(i-1),-e.getDepth());
            }
            quad(vectors.get(vcount-1),vectors.get(0),-e.getDepth());
        }
        glEnd();
        glPopMatrix();

        if(e.isCreative()){
            glPushMatrix();
            glTranslatef(e.getPosition().x,e.getPosition().y,Entity.PRIORITY_7);
            glRotatef((float)Math.toDegrees(e.getRotation().x),1,0,0);
            glRotatef((float)Math.toDegrees(e.getRotation().y),0,1,0);
            glRotatef((float)Math.toDegrees(e.getRotation().z),0,0,1);
            if(e.isOkayShape()){
                drawPolygon(e.getVectors(),0,1,0,0.8f);
            }else {
                drawPolygon(e.getVectors(),1,0,0,0.8f);
            }
            glPopMatrix();

            glPushMatrix();
            glTranslatef(e.getPosition().x,e.getPosition().y,e.getPosition().z+0.01f);
            quad(0.2f,0,1,0,1);
            glPopMatrix();
        }
    }
    /**
     * Sets the render parameters depending on the displays size
     * @param w width of window
     * @param h height of window
     */
    private void reshape (int w, int h)
    {
        glViewport(0, 0, w, h);
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        gluPerspective(100, (float)w/h, 1f, 100.0f);  //Set perspective n deep
    }

    /**
     * Redraws the model
     * @throws ModelNotSetException if there is no mdel
     */
    public void update() throws ModelNotSetException {
        if(!viewSet)throw new ModelNotSetException();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear

        /*Draw EVERYTHING*/
        List<Drawable> drawables = model.getDrawables();
        Collections.sort(drawables);
        for(Drawable d:drawables){
            drawDrawable(d);
        }

        glFlush();//Gt it all out there (necessary?? fixed by Display.update() maby

        /*Set the camera*/
        camera.lookAt();
        Display.update();   //Update le display
    }

    /**
     * Exception incase of no model set
     */
    public class ModelNotSetException extends Exception {

    }

    /* Helper methods for setting proper color*/
    public static void setColor(int red, int green, int blue) {
        glColor3f(red/255f, green/255f, blue/255f);
    }
    public static void setColor(int red, int green, int blue, int alpha) {
        glColor4f(red/255f, green/255f, blue/255f, alpha/255);
    }
    public static void setColor(org.newdawn.slick.Color c) {
        glColor4f(c.getRed()/255f, c.getGreen()/255f, c.getBlue()/255f, c.getAlpha()/255f);
    }
    private static void setColor(java.awt.Color c) {
        glColor4f(c.getRed()/255f, c.getGreen()/255f, c.getBlue()/255f, c.getAlpha()/255f);
    }

    /*Helper methods for drawing shapes*/
    private static void drawPolygon(List<Vec2> vectors, float r, float g, float b, float a){

        glBegin(GL_POLYGON);
        glColor4f(r, g, b, a);
        for(Vec2 v:vectors){
            glVertex3f(v.x,v.y,0.001f);
        }
        glEnd();
    }
    public static void quad(Vec2 v1,Vec2 v2,float d, float r, float g, float b){

        glPushAttrib(GL_CURRENT_BIT);
        glColor3f(r, b, g);    // Color x
        quad(v1, v2, d);
    }

    private static void quad(Vec2 v1, Vec2 v2, float d){
        Vec3 vectors[] = new Vec3[4];
        vectors[0] = new Vec3(v1.x, v1.y, 0);
        vectors[1] = new Vec3(v1.x, v1.y, d);
        vectors[2] = new Vec3(v2.x, v2.y, d);
        vectors[3] = new Vec3(v2.x, v2.y, 0);

        glBegin(GL_QUADS);        // Draw The Cube Using quads
        glTexCoord2f(0,0);
        glVertex3f(vectors[0].x, vectors[0].y, vectors[0].z);
        glTexCoord2f(0,d);
        glVertex3f(vectors[1].x, vectors[1].y, vectors[1].z);
        glTexCoord2f(Helpers.distance(v1, v2)/2,d/2);
        glVertex3f(vectors[2].x, vectors[2].y, vectors[2].z);
        glTexCoord2f(Helpers.distance(v1,v2)/2,0);
        glVertex3f(vectors[3].x,vectors[3].y,vectors[3].z);
        glEnd();
    }

    private static void quad(float s, float r, float g, float b, float a){
        glColor4f(r, b , g,a);    // Color x
        quad(s);
    }

    private static void quad(float s){
        glBegin(GL_QUADS);        // Draw The Cube Using quads
        glVertex2f(s/2, s/2);    // Top Right Of The Quad (Front)
        glVertex2f(-s/2, s/2);    // Top Left Of The Quad (Front)
        glVertex2f(-s/2, -s/2);    // Bottom Left Of The Quad (Front)
        glVertex2f(s / 2, -s / 2);    // Bottom Right Of The Quad (Front)
        glEnd();

    }
    public static void cube(float s) {
        glBegin(GL_QUADS);        // Draw The Cube Using quads
        glVertex3f(s, s, 0);    // Top Right Of The Quad (Top)
        glVertex3f(0, s, 0);    // Top Left Of The Quad (Top)
        glVertex3f(0, s, s);    // Bottom Left Of The Quad (Top)
        glVertex3f(s, s, s);    // Bottom Right Of The Quad (Top)
        glVertex3f(s, 0, s);    // Top Right Of The Quad (Bottom)
        glVertex3f(0, 0, s);    // Top Left Of The Quad (Bottom)
        glVertex3f(0, 0, 0);    // Bottom Left Of The Quad (Bottom)
        glVertex3f(s, 0, 0);    // Bottom Right Of The Quad (Bottom)
        glVertex3f(s, s, s);    // Top Right Of The Quad (Front)
        glVertex3f(0, s, s);    // Top Left Of The Quad (Front)
        glVertex3f(0, 0, s);    // Bottom Left Of The Quad (Front)
        glVertex3f(s, 0, s);    // Bottom Right Of The Quad (Front)
        glVertex3f(s, 0, 0);    // Top Right Of The Quad (Back)
        glVertex3f(0, 0, 0);    // Top Left Of The Quad (Back)
        glVertex3f(0, s, 0);    // Bottom Left Of The Quad (Back)
        glVertex3f(s, s, 0);    // Bottom Right Of The Quad (Back)
        glVertex3f(0, s, s);    // Top Right Of The Quad (Left)
        glVertex3f(0, s, 0);    // Top Left Of The Quad (Left)
        glVertex3f(0, 0, 0);    // Bottom Left Of The Quad (Left)
        glVertex3f(0, 0, s);    // Bottom Right Of The Quad (Left)
        glVertex3f(s, s, 0);    // Top Right Of The Quad (Right)
        glVertex3f(s, s, s);    // Top Left Of The Quad (Right)
        glVertex3f(s, 0, s);    // Bottom Left Of The Quad (Right)
        glVertex3f(s, 0, 0);    // Bottom Right Of The Quad (Right)
        glEnd();            // End Drawing The Cube
    }
}
