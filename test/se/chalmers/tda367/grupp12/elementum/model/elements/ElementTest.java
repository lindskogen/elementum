package se.chalmers.tda367.grupp12.elementum.model.elements;

import org.jbox2d.common.Settings;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import org.junit.Test;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.model.Element;
import se.chalmers.tda367.grupp12.elementum.model.Fire;
import se.chalmers.tda367.grupp12.elementum.model.Player;
import se.chalmers.tda367.grupp12.elementum.view.View;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Haggan
 * Date: 2013-04-15
 * Time: 11:31
 * To change this template use File | Settings | File Templates.
 */
public class ElementTest {

	private Element e;

	@Test
	public void testElement() throws Exception {
		e = new Fire();
		assertTrue(e!=null);
	}

	@Test
	public void testGetFixture() throws Exception {
		e = new Fire();
		assertEquals(e.getFixture().getClass(), FixtureDef.class);
	}

	@Test
	public void testMove() throws Exception {
		View v = new View(false);
		Settings.maxPolygonVertices = 100;
		Player p = new Player(new World(new Vec2(0,0), true),new Vec2(0,0), new Elementum());

		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.switchElement(Player.Elements.FIRE);
		p.left();
		System.out.println(p.getBody().getLinearVelocity());
		assertTrue(p.getBody().getLinearVelocity() != new Vec2(0,0));
		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.right();
		assertTrue(p.getBody().getLinearVelocity().x < 0);

		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.switchElement(Player.Elements.WIND);
		p.left();
		assertTrue(p.getBody().getLinearVelocity().x > 0);
		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.right();
		assertTrue(p.getBody().getLinearVelocity().x < 0);

		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.switchElement(Player.Elements.WATER);
		p.left();
		assertTrue(p.getBody().getLinearVelocity().x > 0);
		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.right();
		assertTrue(p.getBody().getLinearVelocity().x < 0);

		p.getBody().setLinearVelocity(new Vec2(0,0));
		p.switchElement(Player.Elements.EARTH);
		p.left();
		assertTrue(p.getBody().getAngularVelocity() < 0);
		p.getBody().setAngularVelocity(0);
		p.right();
		assertTrue(p.getBody().getAngularVelocity() > 0);
	}

	@Test
	public void testDraw() throws Exception {

	}

}
