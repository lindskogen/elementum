package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Date: 2013-04-11
 * Time: 17:37
 */
public class LevelFactoryTest {
	@Test
	public void testCreateLevel() throws Exception {

	}

	@Test
	public void testOpenLevel() throws Exception {
		Level lvl = LevelFactory.createLevel();

		List<Entity> ents = lvl.getEntities();

		Entity e1 = ents.remove(0);
		assertTrue(e1 instanceof Rock);
		assertEquals(new Vec2(2, 5.5f), e1.getBody().getPosition());

		Entity e2 = ents.remove(0);
		assertTrue(e2 instanceof Rock);
		assertEquals(new Vec2(4, 3.5f), e2.getBody().getPosition());

		Entity e3 = ents.remove(0);
		assertTrue(e3 instanceof LevelWater);
		assertEquals(new Vec2(0.44f, 6.5f), e3.getBody().getPosition());
	}
}
