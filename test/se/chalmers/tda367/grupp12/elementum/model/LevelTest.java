package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Date: 2013-04-11
 * Time: 17:36
 */
public class LevelTest {
	@Test
	public void testAddEntity() throws Exception {
		LevelProperties lp = new LevelProperties(new Vec2(0,-9.8f));
		Level lvl = new Level("test", lp);
		Rock rock = new Rock(lvl, new Vec2(2,3));
		assertTrue(lvl.addEntity(rock));

		assertEquals(rock, lvl.getEntities().get(0));
	}

	public void testGetEntities() {
		LevelProperties lp = new LevelProperties(new Vec2(0,-9.8f));
		Level lvl = new Level("test", lp);
		Rock rock = new Rock(lvl, new Vec2(2,3));
		lvl.addEntity(rock);

		assertEquals(rock, lvl.getEntities().get(0));
	}
}
