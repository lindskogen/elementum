package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 5/26/13
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class EditorCameraTest {
	@Test
	public void testMove() throws Exception {
		EditorCamera editCam = new EditorCamera();
		Vec3 orig = editCam.getCameraPosition();

		Vec2 moveVector = new Vec2(1,1);

		editCam.move(moveVector.x, moveVector.y);

		Vec3 newPos = editCam.getCameraPosition();
		assertTrue(orig.x + moveVector.x == newPos.x);
		assertTrue(orig.y + moveVector.y == newPos.y);
	}

	@Test
	public void testTilt() throws Exception {
		EditorCamera editCam = new EditorCamera();
		Vec3 tiltBefore = editCam.getTilt();

		// Tilt the camera
		Vec2 tilt = new Vec2(2,2);
		editCam.tilt(tilt.x, tilt.y);
		Vec3 tiltAfter = editCam.getTilt();
		assertTrue(tiltBefore.x+tilt.x == tiltAfter.x);
		assertTrue(tiltBefore.y+tilt.y == tiltAfter.y);

		// And tilt it back
		Vec2 negTilt = new Vec2(-2,-2);
		editCam.tilt(negTilt.x, negTilt.y);
		Vec3 tiltAfter2 = editCam.getTilt();
		assertTrue(tiltBefore.x == tiltAfter2.x);
		assertTrue(tiltBefore.y == tiltAfter2.y);
	}

	@Test
	public void testResetTilt() throws Exception {
		EditorCamera editCam = new EditorCamera();
		Vec3 tiltBefore = editCam.getTilt();

		// Tilt the camera
		Vec2 tilt = new Vec2(2,2);
		editCam.tilt(tilt.x, tilt.y);

		// has it changed
		assertFalse(tiltBefore.x == editCam.getTilt().x);
		assertFalse(tiltBefore.y == editCam.getTilt().y);

		// now reset the tilt
		editCam.resetTilt();
		Vec3 tiltAfter = editCam.getTilt();
		assertTrue(tiltBefore.x == tiltAfter.x);
		assertTrue(tiltBefore.y == tiltAfter.y);
	}

	@Test
	public void testZoom() throws Exception {
		EditorCamera editCam = new EditorCamera();
		float zoomBefore = editCam.getCameraPosition().z;

		// Zoom in
		editCam.zoom(2);

		assertTrue(zoomBefore + 2 == editCam.getCameraPosition().z);

		// Zoom out
		editCam.zoom(-2);
	}

	// The method getCameraPosition() is considerd to be tested if tests above asserts true.
}
