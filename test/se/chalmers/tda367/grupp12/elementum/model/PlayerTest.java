package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Settings;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.World;
import org.junit.Test;
import org.lwjgl.opengl.Display;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.view.View;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;

/**
 * Date: 4/11/13
 * Time: 5:11 PM
 */
public class PlayerTest {
    @Test
    public void testPlayer() throws Exception{
        Player p = new Player(new World(new Vec2(),false),new Vec2(), new Elementum());
        assertTrue(p.getCurrentElement() != null);
    }

    @Test
    public void testSwitchElement() throws Exception {
	    View v = new View(false);
	    Settings.maxPolygonVertices = 100;
	    Player p = new Player(new World(new Vec2(),true),new Vec2(), new Elementum());
        Body b=new Body(new BodyDef(),p.getBody().getWorld());


        //Check all switches
        p.switchElement(Player.Elements.EARTH);
        assertEquals(p.getCurrentElement().getClass(), Earth.class);

        p.switchElement(Player.Elements.FIRE);
        assertTrue(p.getCurrentElement() instanceof Fire);

        p.switchElement(Player.Elements.WATER);
        assertTrue(p.getCurrentElement() instanceof Water);

        p.switchElement(Player.Elements.WIND);
        assertTrue(p.getCurrentElement() instanceof Wind);

        //Check fixture changed
        //Equals not implemented, cannot compare
        //assertEquals(p.getBody().getFixtureList(), b.createFixture(p.getCurrentElement() .getFixture()));
    }

    @Test
    public void testGetState() throws Exception {
		Player p = new Player(new World(new Vec2(), true),new Vec2(), new Elementum());

	    p.switchElement(Player.Elements.EARTH);
	    assertEquals(p.getState(), Player.Elements.EARTH);

	    p.switchElement(Player.Elements.FIRE);
	    assertEquals(p.getState(), Player.Elements.FIRE);

	    p.switchElement(Player.Elements.WATER);
	    assertEquals(p.getState(), Player.Elements.WATER);

	    p.switchElement(Player.Elements.WIND);
	    assertEquals(p.getState(), Player.Elements.WIND);

    }

	@Test
	public void testAvailableDirections() throws Exception {
		Player p = new Player(new World(new Vec2(), true),new Vec2(), new Elementum());
		boolean[] b = new boolean[]{false, false, false, false};
		assertNotSame(p.getAvailableDirections(),b);
		p.setAvailableDirections(b);
		assertEquals(p.getAvailableDirections(), b);
	}
}
