package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 4/11/13
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class EntityTest {
	Vec2 vec = new Vec2();
	World world = new World(vec,true);
	Rock rock = new Rock(world,vec);

	private void createRockDef(Rock rocker, float w, float h){
		FixtureDef fixtD = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(w,h);
		fixtD.shape = shape;
		rocker.createFixtureDef(fixtD);
	}

	/*public void testGetPolygon() throws Exception {
		//create a 3x3m rock
		createRockDef(rock, 3f,3f);

		//Create a Polygon similar to rocks polygon
		Polygon polygon = new Polygon();
		Vec2[] vecList = ((PolygonShape)(rock.getBody().getFixtureList().getShape())).getVertices();
		for(Vec2 vec: vecList){
			polygon.addPoint((int)vec.x,(int)vec.y);
		}

		//int[] rockX = rock.getPolygon().xpoints;
		//int[] rockY = rock.getPolygon().ypoints;
		int[] polX = polygon.xpoints;
		int[] polY = polygon.ypoints;

		for (int i = 0; i<rockX.length; i++){
			assertTrue(rockX[i] == polX[i]);
			assertTrue(rockY[i] == polY[i]);
		}
	}
	*/

	@Test
	public void testGetBody() throws Exception {

		Body rockBody = rock.getBody();
		assertTrue(rockBody.getType() == BodyType.STATIC);
		assertTrue(rockBody.getPosition().x == vec.x);
		assertTrue(rockBody.getPosition().y == vec.y);
		assertTrue(rockBody.getWorld() == world);
	}
    /*
	@Test
	public void testContains() throws Exception {
		// create a rock with dimensions 3 by 3
		createRockDef(rock,3f,3f);

		//This rock is smaller than the original, and should be contained
		Rock rock2 = new Rock(world,vec);
		FixtureDef fixt = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(2f,2f);
		fixt.shape = shape;
		rock2.getBody().createFixture(fixt);

		assertTrue(rock.contains(rock2));

		//This rock is bigger than the original, and should not be contained
		Rock rock3 = new Rock(world,vec);
		FixtureDef fixt3 = new FixtureDef();
		PolygonShape shape3 = new PolygonShape();
		shape3.setAsBox(8f, 8f);
		fixt3.shape = shape3;
		rock3.getBody().createFixture(fixt3);

		assertFalse(rock.contains(rock3));
	}

	@Test
	public void testContainsAnyPointOf() throws Exception {
		// original rock at 0,0
		createRockDef(rock, 3f,3f);

		// Outside of original rock
		Vec2 v = new Vec2(9f,9f); // Does'nt seem to affect.. . look into this.
		Rock outAndBicyclingRock = new Rock(world,v);
		createRockDef(outAndBicyclingRock, 3f,3f);
		assertFalse(rock.containsAnyPointOf(outAndBicyclingRock));

		// partly inside original rock
		Vec2 v2 = new Vec2(0.1f,0.1f);
		Rock inAndBicyclingRock = new Rock(world,v2);
		createRockDef(inAndBicyclingRock, 3f,3f);
		assertTrue(rock.containsAnyPointOf(inAndBicyclingRock));

	}

	@Test
	public void testIntersects() throws Exception {

		// original rock at 0,0
		createRockDef(rock, 3f,3f);

		//Goes through rock
		Rock throughRock = new Rock(world, vec);
		createRockDef(throughRock, 1f, 4f);
		assertTrue(rock.intersects(throughRock));


		// Does'nt go through rock
		Vec2 v = new Vec2(9f,9f); // Does'nt seem to affect.. . look into this.
		Rock outAndBicyclingRock = new Rock(world,v);
		createRockDef(outAndBicyclingRock, 3f,3f);
		assertFalse(rock.intersects(outAndBicyclingRock));
	}*/

	@Test
	public void testDraw() throws Exception {

	}

	@Test
	public void testUpdate() throws Exception {

	}
}
