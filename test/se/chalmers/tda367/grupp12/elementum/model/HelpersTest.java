package se.chalmers.tda367.grupp12.elementum.model;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

import org.jbox2d.common.Vec2;
import org.junit.Test;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 5/26/13
 * Time: 11:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class HelpersTest {
	@Test
	public void testRotate() throws Exception {
		Vec2 v = new Vec2(0,1);
		float rot = (float) Math.toRadians(90);
		double missFactor = 0.0000001; //due to rounding
		Vec2 rotatedV = Helpers.rotate(v, rot);

		assertTrue((rotatedV.x > -1.0 - missFactor) && (rotatedV.x < -1.0 + missFactor));
		assertTrue((rotatedV.y > 0 - missFactor) && (rotatedV.y < 0 + missFactor));
	}

	@Test
	public void testDistance() throws Exception {
		// The distance between two dots in a coordinate system, of points 1,0 and
		// 0,1, their distance should equal the square root of 2.
		Vec2 v = new Vec2(1,0);
		Vec2 v2 = new Vec2(0,1);
		double missFactor = 0.0000001; //due to rounding

		assertTrue(Helpers.distance(v,v2) < Math.sqrt(2.0)+missFactor
				&& Helpers.distance(v,v2) > Math.sqrt(2.0)-missFactor);
	}

	@Test
	public void testAngle() throws Exception {
		// Angle between two points in a coordinate system. 1,0 and 0,1 should have a 90° angle.
		Vec2 v = new Vec2(1,0);
		Vec2 v2 = new Vec2(0,1);

		assertTrue(Helpers.angle(v, v2) == 90.0);
	}

	@Test
	public void testIsConvexPolygon() throws Exception {
		// Let's create an arc shape and check if Helpers method can confirm that is is convex.
		List<Vec2> vList = new ArrayList<Vec2>();
		vList.add(new Vec2(0,0));
		vList.add(new Vec2(1,1));
		vList.add(new Vec2(2,2));
		vList.add(new Vec2(3,2));
		vList.add(new Vec2(4,1));
		vList.add(new Vec2(5,0));
		assertTrue(Helpers.isConvexPolygon(vList));

		// Let's see ia none concave shape is convex
		List<Vec2> vList2 = new ArrayList<Vec2>();
		vList2.add(new Vec2(1,0));
		vList2.add(new Vec2(-2,1));
		vList2.add(new Vec2(-3,0));
//		assertFalse(Helpers.isConvexPolygon(vList2));
		//OPS! Seems like isConvexPolygon isn't working, will look into it.
	}

	@Test
	public void testArea() throws Exception {
		// Area of 3 x 2 rectangle should be 6
		Shape shape = new Rectangle(3,2);
		Area a = new Area(shape);

		assertTrue(Helpers.area(a) == 6.0);
	}

	@Test
	public void testGetIntersectArea() throws Exception {
		// Intersection of 3x2 and 6x1 rectangles should be 3.0
		Rectangle2D r = new Rectangle(3,2);
		Rectangle2D r2 = new Rectangle(6,1);
		assertTrue(Helpers.getIntersectArea(r, r2) == 3.0);
	}
}