package se.chalmers.tda367.grupp12.elementum.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.common.Vec3;
import org.jbox2d.dynamics.World;
import org.junit.Test;
import se.chalmers.tda367.grupp12.elementum.Elementum;

import java.awt.event.ActionListener;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Patrik
 * Date: 5/26/13
 * Time: 11:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerCameraTest {
	/*psuedo elements to create a camera*/
	private World w = new World(new Vec2(0,0),false);
	private Player p = new Player(w, new Vec2(0,0), new Elementum());
	private PlayerCamera pc = new PlayerCamera(p);

	@Test
	public void testSetPlayer() throws Exception {
		try{
			pc.setPlayer(p);
		}catch (Exception e){
			assertTrue(false);
		}
	}

	@Test
	public void testUpdateCamera() throws Exception {
		Vec3 cameraBefore = pc.getCameraPosition();
		Vec2 v = new Vec2(p.getPosition().x, p.getPosition().y);

		Vec2 newPos = new Vec2(10,4);
		p.setPosition(newPos);
		pc.updateCamera();
		Vec3 cameraAfter = pc.getCameraPosition();
		assertTrue(cameraBefore.x != cameraAfter.x);
		assertTrue(cameraBefore.y != cameraAfter.y);
	}
}
