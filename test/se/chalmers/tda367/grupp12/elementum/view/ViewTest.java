package se.chalmers.tda367.grupp12.elementum.view;

import org.junit.Test;
import se.chalmers.tda367.grupp12.elementum.Elementum;
import se.chalmers.tda367.grupp12.elementum.model.Universe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Date: 4/12/13
 * Time: 11:51 AM
 *
 * At the moment, this tests like everything. Run for action ^^
 */
public class ViewTest {
    @Test
    public void testUpdate() throws Exception {
        View view = new View(true);
        Universe u = new Universe(new Elementum());
        view.setModel(u);

        while(true){
            u.update();
            view.update();
        }
    }
}
