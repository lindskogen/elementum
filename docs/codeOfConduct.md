# Code of conduct
## About 
This document outlines the responsibilities and proper practises of the group as hole and it's individuals regarding work and group dynamics in specific situations as well as generall guidelines.
## Actions to be taken when the following issue is recognised by one or several individuals in the group
-  A person is being to talkative or to dominant

The individual or individuals recognizing the issue shall imediatly bring this to light to the rest of the group in a constructive manner. The situation is then disscussed and resolved by taking appropriate action.

- A person is overly quiet and nonparticipating 

The other members tries to involve the person by giving him room to express his thoughts by for example asking questions. If the matter is regarded as serious the matter should be brought in to light to the entire group and we discuss the matter to recognize possible issues while respecting that some individuals can be less talkactive to their nature.

- A person is annoyed or angry att something or someone

The issue shall immidiatl be brought to light and discussed by the group as whole and appropriate action(s) shall be taken. 

- Difficulty to agree upon a matter

Compromize, with the long-term perspective in mind, using democrasy.

- Someone is breaking the Code of Conduct

Imidiate discussion with the entire group and take apropriate action. Oe action can be to revise the Code of Conduct. In extreme cases condact the course administrator.

## General guidelines
### How we acheive a great working climate
- Continious communication
- Respect, aid and encourage one another
- Take immidiate action when recognizing a issue
### Distribution of work
- According to the capabilities of the individuals
- According to what the individuals want to do
- Dynamic work assignments depending on progress
### Ambition level
- High ambitions
- Striving for high quality result
- Striving for highest possible grade
- Time spend around 30 - 40 hours a week minimum