# Meeting Agenda

Date: 2013-05-14

Facilitator: Johan Lindskogen

Participants: Johan Hage, Johan Lindskogen, Christopher Åkersten, Patrik Göthe

## 1. Objectives
- **Johan L** Level structure
- **Johan H** Interactables
- **Christopher** Balancing elements and interactables (water).
- **Patrik** Level structure, Menu system


## 2. Reports
- **Johan L** Done
- **Johan H** Almost done.
- **Christopher**  Done.
- **Patrik Göthe** Done and Components done, just connecting them that remains.


## 3. Discussion items
### More levels
- Atleast five levels

### saving
- Be able to save ones progress

### Menusystem
In game
- Resume
- Exit
- Select level
Main menu ->
- Continue
- New game
- Level editor
- Select level
- Exit

### Music & sound
- Narrator?
- Legal

### Images
- Check legal

### Presentation
- Visual
- demo

### Docs
- SDD
- RAD
- Report

### Testing

## 4. Outcomes and assignments
- **Johan L** Progress saving, check image legality, documentation
	- To review: Entity, Level Objects/*

- **Johan H** Level implementation, documentation
	- To review: Element, Water, Earth, Wind, Fire, Audio, Player
	- 
- **Christopher** Level implementation, documentation
	- To review: ContentImage, Controller, FileManager, Menu System
	- 
- **Patrik** Menusystem, Music, documentation
- To review: Camera *, Model, View, Elementum, Main, Level, LevelProperties, LevelFactory, FPSManager, Universe
- 
## 5. Wrap up
Next meeting: Last meeting