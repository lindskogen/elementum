# Meeting Agenda

Date: 2013-03-19

Facilitator: Patrik Göthe

Participants: Johan Lindskogen, Christopher Åkersten, Patrik Göthe

# 1. Objectives
We need to do some research in how platform games are developed using java, then by these results decide what libraries to use in this project.

# 2. Reports
Idea and concept is defined. Scope is still to be defined.

# 3. Discussion items 

## How do we perform the neccesary research?
Ask around, search the web, experiment with libraries.

## How do we store levels?
By bitmapping, images and a collision map

## What are the biggest challanges in this project?
Level presentation/Storing/Layering
Character properties/abilities
Physics engine
Defining gameplay

# 4. Outcomes and assignments 
Everyone is to read up on the libraries LWJGL, Slick2D, JBox2D.

Then will try to have a hackathon, with the purpose of familiarizing ourselves with the diffrent libraries, and see what we can accomplish during one evening, to get a grasp on what we are getting into.

The outcome of this hackathon will help us decide what library we will be applying to our game.

# 5. Wrap up 

The library which is to be used.

Next meeting: Wednesday