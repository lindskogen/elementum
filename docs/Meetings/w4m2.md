# Meeting Agenda

Date: 2013-04-25

Facilitator: Johan Lindskogen

Participants: Johan Hage, Johan Lindskogen, Christopher Åkersten, Patrik Göthe

## 1. Objectives
- Continues world normalization. 
- Camera. 
- Creates entities.  
- Continue implementation of level editor.
- Menu systems
- Saving levels

## 2. Reports
- **Johan H:**
  - World normalization
    - Jumping still not optimal.
  - Camera
    - Still not working, should be percentage of screen size.
    - Still in development.
  - Create entities
    - Still in development.
- **Christopher:**
  - Level editor
    - Declared as done!
    - Next step: Implement more entities and save levels!
    - Not in active development any more.
- **Patrik:**
  - Menu system
    - Needs way of switching reciever in the controller.
    - Camera not 
- **Johan:**
  - Saving levels
    - Completed, not with FileManager though.


## 3. Discussion items

### Jump physics
- Collision zone underneath which detects jumpability

### Menus and Controllers
- Elementum is a listener to the controller
- Controls what `InputAction` should react to controller input

### FileManager walkthrough
- Level management, `getLevel`returns `File`-object and will be used to write/read levels to/from.


## 4. Outcomes and assignments
**Johan H:** Jump physics.  
**Christopher:** Controller.  
**Patrik Göthe:** Menu systems + FileManager  
**Johan L:** Controller + Menu system  

## 5. Wrap up
Next meeting: _Tuesday_