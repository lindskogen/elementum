# Meeting Agenda

Date: 2013-04-30

Facilitator: Patrik Göthe

Participants: Johan Hage, Johan Lindskogen, Christopher Åkersten, Patrik Göthe

## 1. Objectives
Johan H: Jump physics.
Christopher: Controller.
Patrik Göthe: Menu systems + FileManager
Johan L: Controller + Menu system


## 2. Reports
- **Johan H** Solved Jump physics.
- **Christopher** Controller still not fully implemented.
- **Patrik Göthe** Still some implementation/merging todo.
- **Johan L** Ongoing.

## 3. Discussion items
### Project structure
- How do we proceed more efficiently?
    - Need to have more specified tasks/sprints.
    - Fill up trello with good assigments.
    - More focused on the project in group sessions

### What is the next step?
- Creating a level - designing the first puzzle

### Boolean element available for toggling
- A way to control whether elements may be switched to
    - eg. in a crack you will not be able to change into any element.

### Breakable objects
- Implement breakable objects
- Joints?
- Many objects?

### Texture handling
- Transparency
- Grass, etc.

## 4. Outcomes and assignments
- **Johan L** (Boolean element toggling), start creating _the first level_.
- **Johan H** Breakable objects.
- **Christopher** Texture handling.
- **Patrik** Menu system, level creation.

## 5. Wrap up
Next meeting: Thursday or Friday