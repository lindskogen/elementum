Elementum
=========

Game about elements for course [TDA367].

To run add all in lib folder to library path

Run with VM parameters: -Djava.library.path=natives to link the natives

You should be good to go!

Controls
========

W,A,S,D - Jump, down, left, right

Confirm choices with enter.

Change Elements with 1, 2, 3, 4.

Open menu with Esc.


Notes
=====

In the repo Ciff is actually Christopher.
